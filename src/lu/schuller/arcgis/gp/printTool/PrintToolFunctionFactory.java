/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.schuller.arcgis.gp.printTool;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 27/03/12
 * Time: 21:46
 */

import com.esri.arcgis.geodatabase.IEnumGPName;
import com.esri.arcgis.geodatabase.IGPName;
import com.esri.arcgis.geoprocessing.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.interop.extn.ArcGISCategories;
import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.UID;

import java.io.IOException;
import java.util.UUID;

@ArcGISExtension(categories = {ArcGISCategories.GPFunctionFactories})
public class PrintToolFunctionFactory implements IGPFunctionFactory {

    private String functionFactoryAlias = "printToolFunctionFactory";
    private String factoryName = "PrintToolFunctionFactory";
    private String toolset = "PChPrintSOEToolsetPrint";
    private String toolName = "PrintTool";
    private String displayName = "PrintTool";
    private String description = "advanced Printing tool";

    public IGPFunction getFunction(String name) throws IOException, AutomationException {
        if (name.equalsIgnoreCase(toolName))
            return new PrintTool();
        return null;
    }

    public IGPName getFunctionName(String name) throws IOException, AutomationException {
        if (name.equalsIgnoreCase(toolName)) {
            GPFunctionName functionName = new GPFunctionName();
            functionName.setCategory(toolset);
            functionName.setDescription(description);
            functionName.setDisplayName(displayName);
            functionName.setName(toolName);
            //functionName.setMinimumProduct(esriProductCode.esriProductCodeAdvanced);
            functionName.setFactoryByRef(this);
            return functionName;
        }
        return null;
    }

    public IEnumGPName getFunctionNames() throws IOException, AutomationException {
        EnumGPName nameArray = new EnumGPName();
        nameArray.add(getFunctionName(toolName));
        return nameArray;
    }

    public String getAlias() throws IOException, AutomationException {
        return functionFactoryAlias;  // lower case of the FunctionFactory name.
    }

    public IUID getCLSID() throws IOException, AutomationException {
        UID uid = new UID();
        uid.setValue("{" + UUID.nameUUIDFromBytes(this.getClass().getName().getBytes()) + "}");
        return uid;
    }

    public IEnumGPEnvironment getFunctionEnvironments() throws IOException, AutomationException {
        return null;
    }

    public String getName() throws IOException, AutomationException {
        return factoryName;
    }

}
