/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.schuller.arcgis.gp.printTool;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 27/03/12
 * Time: 21:46
 */

import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geodatabase.IGPMessages;
import com.esri.arcgis.geodatabase.IGPValue;
import com.esri.arcgis.geodatabase.esriGPMessageSeverity;
import com.esri.arcgis.geoprocessing.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.Array;
import com.esri.arcgis.system.IArray;
import com.esri.arcgis.system.IName;
import com.esri.arcgis.system.ITrackCancel;
import lu.etat.pch.gis.soe.tasks.print.PrintTask;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

public class PrintTool extends BaseGeoprocessingTool {
    private static final String TAG = "PrintTool";
    private String toolName = "PrintTool";
    private String displayName = "PrintTool";
    private String metadataFileName = "lu.schuller.printTask.PrintTool.xml";

    public PrintTool() {

    }

    public String getName() throws IOException, AutomationException {
        return toolName;
    }

    public String getDisplayName() throws IOException, AutomationException {
        return displayName;
    }

    public IName getFullName() throws IOException, AutomationException {
        return (IName) new PrintToolFunctionFactory().getFunctionName(toolName);
    }

    public IArray getParameterInfo() throws IOException, AutomationException {
        IArray parameters = new Array();

        GPParameter mxdParameter = new GPParameter();
        mxdParameter.setName("mxdFile");
        mxdParameter.setDirection(esriGPParameterDirection.esriGPParameterDirectionInput);
        mxdParameter.setDisplayName("mxdFile");
        mxdParameter.setParameterType(esriGPParameterType.esriGPParameterTypeRequired);
        mxdParameter.setDataTypeByRef(new GPStringType());
        GPString mxdGPString = new GPString();
        mxdGPString.setValue("D:\\arcgisserver_data\\layout\\layoutA4.mxd");
        mxdParameter.setValueByRef(mxdGPString);
        parameters.add(mxdParameter);

        GPParameter inputParameter = new GPParameter();
        inputParameter.setName("input");
        inputParameter.setDirection(esriGPParameterDirection.esriGPParameterDirectionInput);
        inputParameter.setDisplayName("input");
        inputParameter.setParameterType(esriGPParameterType.esriGPParameterTypeRequired);
        inputParameter.setDataTypeByRef(new GPStringType());
        GPString inputGPString = new GPString();
        inputGPString.setValue("http://localhost:6080/arcgis/rest/services/layout/portraitA4/MapServer/exts/PCHPrintSOE/printLayout?mapExtent={\"xmin\":45809.058683766925,\"ymin\":63983.902135614335,\"xmax\":105809.05868376693,\"ymax\":150083.90213561433,\"spatialReference\":{\"wkid\":2169}}&printOutput={\"height\":29.7,\"format\":\"pdf\",\"mapRotation\":0,\"pageUnits\":\"cm\",\"resolution\":100,\"exportSettings\":null,\"borderWidth\":[0.5,0.5,0.5,0.5],\"refererWebsite\":null,\"referenceScale\":0,\"toRemoveLayoutElements\":[\"*\"],\"width\":21}&mapElements=[]&layoutElements=[]&mapServices=[{\"name\":\"LimAdm\",\"type\":\"AGS\",\"url\":\"http://localhost:6080/arcgis/rest/services/sig/limadm/MapServer\",\"definitionExpression\":null,\"alpha\":1,\"token\":\"\",\"visibleIds\":\"1,3,4,6\"}]");
        inputParameter.setValueByRef(inputGPString);
        parameters.add(inputParameter);

        GPParameter anwerParameter = new GPParameter();
        anwerParameter.setName("answer");
        anwerParameter.setDirection(esriGPParameterDirection.esriGPParameterDirectionOutput);
        anwerParameter.setDisplayName("answer");
        anwerParameter.setParameterType(esriGPParameterType.esriGPParameterTypeDerived);
        anwerParameter.setDataTypeByRef(new GPStringType());
        anwerParameter.setValueByRef(new GPString());
        parameters.add(anwerParameter);

        return parameters;
    }

    public void updateParameters(IArray paramvalues, IGPEnvironmentManager envMgr) {
        try {
            IGPParameter mxdParamemter = (IGPParameter) paramvalues.getElement(0);
            IGPValue mxdParamemterValue = gpUtilities.unpackGPValue(mxdParamemter);

            File mxdFile = new File(mxdParamemterValue.getAsText());
            if (!mxdFile.canRead()) {
                //todo: .....
            }

            IGPParameter inputParamemter = (IGPParameter) paramvalues.getElement(1);
            IGPValue inputParamemterValue = gpUtilities.unpackGPValue(inputParamemter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called after returning from the internal validation routine. You can examine the messages
     * created from internal validation and change them if desired.
     */
    public void updateMessages(IArray paramvalues, IGPEnvironmentManager envMgr, IGPMessages gpMessages) {
        try {
            if (gpMessages.getMaxSeverity() == esriGPMessageSeverity.esriGPMessageSeverityError) {
                for (int i = 0; i < gpMessages.getCount(); i++) {
                    System.out.println(gpMessages.getMessage(i).getDescription());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void execute(IArray paramvalues, ITrackCancel trackcancel, IGPEnvironmentManager envMgr, IGPMessages messages) throws IOException, AutomationException {
        SOELogger soeLogger2 = new SOELogger(messages, 9000);

        String physicalOutput = null;
        IArray envArray = envMgr.getEnvironments();
        for (int i = 0; i < envArray.getCount(); i++) {
            GPEnvironment obj = (GPEnvironment) envArray.getElement(i);
            soeLogger2.debug(TAG, "obj: " + obj.getName() + "  " + obj.getValue().getAsText());
            if (obj.getName().equalsIgnoreCase("scratchWorkspace")) {
                physicalOutput = obj.getValue().getAsText();
            }
        }
        IGPParameter mxdParamemter = (IGPParameter) paramvalues.getElement(0);
        IGPValue mxdParamemterValue = gpUtilities.unpackGPValue(mxdParamemter);
        String mxdFile = mxdParamemterValue.getAsText();
        IGPParameter inputParamemter = (IGPParameter) paramvalues.getElement(1);
        IGPValue inputParamemterValue = gpUtilities.unpackGPValue(inputParamemter);
        String input = inputParamemterValue.getAsText();

        messages.addMessage("execute.mxdParamemterValue = " + mxdParamemterValue.getAsText());
        messages.addMessage("execute.inputParamemterValue = " + inputParamemterValue.getAsText());
        messages.addMessage("execute.physicalOutput = " + physicalOutput);

        JSONObject jsonObject;
        if (input.contains("handleRESTRequest"))
            jsonObject = getParamsFromLog(input);
        else if (input.startsWith("{") && input.endsWith("}")) {
            jsonObject = new JSONObject(input);
        } else
            jsonObject = getUrlParams(input);
        messages.addMessage("execute.jsonObject = " + jsonObject.toString());

        String answer = null;

        try {
            MapServer mapServer = new MapServer();
            mapServer.connect(mxdFile);
            //String name = mapServer.getDefaultMapName();
            mapServer.getMap(mapServer.getDefaultMapName()).clearLayers();
            messages.addMessage("execute.mapServer: " + mapServer);

            PrintTask printTask = new PrintTask(soeLogger2, mapServer, "printGP");
            messages.addMessage("execute.printTask.physicalOutput: " + physicalOutput);
            if (physicalOutput == null || physicalOutput.trim().length() == 0) {
                messages.addMessage("execute.printTask.TEMP: " + System.getenv("TEMP"));
                printTask.setPhysicalOutputFolder(System.getenv("TEMP"));
            } else {
                printTask.setPhysicalOutputFolder(physicalOutput);
            }
            messages.addMessage("execute.printTask.2: " + printTask);

            if (jsonObject.getString("command").equals("map")) {
                messages.addMessage("execute.printMap...");
                answer = printTask.printMap(jsonObject, true);
            } else if (jsonObject.getString("command").equals("layout")) {
                messages.addMessage("execute.printLayout...");
                answer = printTask.printLayout(jsonObject, true);
            }
            mapServer.release();
        } catch (Exception ex) {
            messages.addMessage("execute.Exception = " + ex.getMessage());
            ex.printStackTrace();
        }
        messages.addMessage("execute.gpTaskAnswer: " + answer);

        IGPParameter answerParameter = (IGPParameter) paramvalues.getElement(2);
        IGPValue answerParameterValue = gpUtilities.unpackGPValue(answerParameter);

        answerParameterValue.setAsText(answer);
        messages.addMessage("execute.answerParameterValue.txt = " + answerParameterValue.getAsText());
        messages.addMessage("execute.DONE");

        messages.addMessage("answer: " + answer);
    }

    public String getMetadataFile() throws IOException, AutomationException {
        return metadataFileName;
    }

    public boolean isLicensed() throws IOException, AutomationException {
        return true;
    }

    private static JSONObject getParamsFromLog(String logEntry) {
        try {
            int posiStart = logEntry.lastIndexOf("operationInput");
            int posiEnd = logEntry.lastIndexOf("outputFormat");
            String params = logEntry.substring(posiStart + 15, posiEnd - 1);
            params = params.replaceAll("&quot;", "\"");
            JSONObject retObj = new JSONObject(params);
            if (logEntry.contains("operationName: printMap")) retObj.put("command", "map");
            if (logEntry.contains("operationName: printLayout")) retObj.put("command", "layout");
            return retObj;
        } catch (StringIndexOutOfBoundsException ex) {
            //try 10.1
            String params = logEntry.substring(logEntry.indexOf('{'), logEntry.lastIndexOf('}') + 1);
            params = params.replaceAll("&quot;", "\"");
            JSONObject retObj = new JSONObject(params);
            if (logEntry.contains("handleRESTRequest.printMap")) retObj.put("command", "map");
            if (logEntry.contains("handleRESTRequest.printLayout")) retObj.put("command", "layout");
            return retObj;
        }
    }

    private static JSONObject getUrlParams(String url) {
        JSONObject jsonObject = new JSONObject();
        if (url.contains("/printLayout?")) jsonObject.put("command", "layout");
        if (url.contains("/printMap?")) jsonObject.put("command", "map");
        String params = url.substring(url.indexOf("?") + 1);
        StringTokenizer st = new StringTokenizer(params, "&", false);
        while (st.hasMoreTokens()) {
            String param = st.nextToken();
            int posi = param.indexOf("=");
            System.out.println("posi = " + posi);
            String paramName = param.substring(0, posi);
            String paramValue = param.substring(posi + 1);
            System.out.println("paramName = " + paramName);
            System.out.println("paramValue = " + paramValue);
            if (paramName.equals("f") && paramValue.equals("json")) {
                //do nothing, ignore
            } else {
                if (paramValue.startsWith("["))
                    try {
                        jsonObject.put(paramName, new JSONArray(paramValue));
                    } catch (JSONException ex) {
                        System.err.println(paramName + " >> " + ex.getMessage());
                    }
                else
                    jsonObject.put(paramName, new JSONObject(paramValue));
            }
        }
        return jsonObject;
    }


}
