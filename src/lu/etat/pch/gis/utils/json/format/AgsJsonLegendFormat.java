/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.format;

import com.esri.arcgis.carto.ILegendFormat;
import com.esri.arcgis.carto.Legend;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/27/12
 * Time: 5:24 PM
 */
public class AgsJsonLegendFormat {
    private static final String TAG = "AgsJsonLegendFormat";
    private SOELogger logger;

    private AgsJsonTextSymbol titleSymbol;
    private boolean showTitle = false;
    private Double titleGab;
    private Double textGab;
    //todo: private Object defaultAreaPatch;
    //todo: private Object defaultLinePatch;
    private Double defaultPatchHeight;
    private Double defaultPatchWidth;
    private Double groupGap;
    private Double headingGap;
    private Double horizontalItemGap;
    private Double horizontalPatchGap;
    private Double layerNameGap;
    //todo: private Integer titlePosition;
    private Double verticalItemGap;
    private Double verticalPatchGap;

    public AgsJsonLegendFormat(SOELogger logger, JSONObject jsonObject) {
        this.logger = logger;
        if (jsonObject == null) return;
        if (!jsonObject.isNull("titleSymbol"))
            this.titleSymbol = new AgsJsonTextSymbol(logger, jsonObject.getJSONObject("titleSymbol"));
        if (!jsonObject.isNull("showTitle"))
            this.showTitle = jsonObject.getBoolean("showTitle");
        if (!jsonObject.isNull("titleGab"))
            this.titleGab = jsonObject.getDouble("titleGab");
        if (!jsonObject.isNull("textGab"))
            this.textGab = jsonObject.getDouble("textGab");
        if (!jsonObject.isNull("defaultPatchHeight"))
            this.defaultPatchHeight = jsonObject.getDouble("defaultPatchHeight");
        if (!jsonObject.isNull("defaultPatchWidth"))
            this.defaultPatchWidth = jsonObject.getDouble("defaultPatchWidth");
        if (!jsonObject.isNull("groupGap"))
            this.groupGap = jsonObject.getDouble("groupGap");
        if (!jsonObject.isNull("headingGap"))
            this.headingGap = jsonObject.getDouble("headingGap");
        if (!jsonObject.isNull("horizontalItemGap"))
            this.horizontalItemGap = jsonObject.getDouble("horizontalItemGap");
        if (!jsonObject.isNull("horizontalPatchGap"))
            this.horizontalPatchGap = jsonObject.getDouble("horizontalPatchGap");
        if (!jsonObject.isNull("layerNameGap"))
            this.layerNameGap = jsonObject.getDouble("layerNameGap");
        if (!jsonObject.isNull("verticalItemGap"))
            this.verticalItemGap = jsonObject.getDouble("verticalItemGap");
        if (!jsonObject.isNull("verticalPatchGap"))
            this.verticalPatchGap = jsonObject.getDouble("verticalPatchGap");
    }

    public ILegendFormat toArcObjects(Legend legend) {
        try {
            ILegendFormat legendFormat = legend.getFormat();
            if (titleSymbol != null) {
                logger.debug(TAG, "toArcObjects.titleSymbol: " + titleSymbol);
                legendFormat.setTitleSymbol((ITextSymbol) titleSymbol.toArcObject().get(0));
            }
            logger.debug(TAG, "toArcObjects.showTitle: " + showTitle);
            legendFormat.setShowTitle(showTitle);
            if (titleGab != null) {
                logger.debug(TAG, "toArcObjects.titleGab: " + titleGab);
                legendFormat.setTitleGap(titleGab);
            }
            if (textGab != null) {
                logger.debug(TAG, "toArcObjects.textGab: " + textGab);
                legendFormat.setTextGap(textGab);
            }
            if (defaultPatchHeight != null) {
                logger.debug(TAG, "toArcObjects.defaultPatchHeight: " + defaultPatchHeight);
                legendFormat.setTextGap(defaultPatchHeight);
            }
            if (defaultPatchWidth != null) {
                logger.debug(TAG, "toArcObjects.defaultPatchWidth: " + defaultPatchWidth);
                legendFormat.setTextGap(defaultPatchWidth);
            }
            if (groupGap != null) {
                logger.debug(TAG, "toArcObjects.groupGap: " + groupGap);
                legendFormat.setTextGap(groupGap);
            }
            if (headingGap != null) {
                logger.debug(TAG, "toArcObjects.headingGap: " + headingGap);
                legendFormat.setTextGap(headingGap);
            }
            if (horizontalItemGap != null) {
                logger.debug(TAG, "toArcObjects.horizontalItemGap: " + horizontalItemGap);
                legendFormat.setTextGap(horizontalItemGap);
            }
            if (horizontalPatchGap != null) {
                logger.debug(TAG, "toArcObjects.horizontalPatchGap: " + horizontalPatchGap);
                legendFormat.setTextGap(horizontalPatchGap);
            }
            if (layerNameGap != null) {
                logger.debug(TAG, "toArcObjects.layerNameGap: " + layerNameGap);
                legendFormat.setTextGap(layerNameGap);
            }
            if (verticalItemGap != null) {
                logger.debug(TAG, "toArcObjects.verticalItemGap: " + verticalItemGap);
                legendFormat.setTextGap(verticalItemGap);
            }

            if (verticalPatchGap != null) {
                logger.debug(TAG, "toArcObjects.verticalPatchGap: " + verticalPatchGap);
                legendFormat.setTextGap(verticalPatchGap);
            }
            return legendFormat;
        } catch (AutomationException e) {
            logger.error(TAG, "toArcObjects.AutomationException", e);
        } catch (IOException e) {
            logger.error(TAG, "toArcObjects.IOException", e);
        }
        return null;
    }

    public JSONObject toJSON() {
        JSONObject symbObject = new JSONObject();
        if (titleSymbol != null) symbObject.put("titleSymbol", titleSymbol.toJSON());
        symbObject.put("showTitle", showTitle);
        if (titleGab != null) symbObject.put("titleGab", titleGab);
        if (textGab != null) symbObject.put("textGab", textGab);
        if (defaultPatchHeight != null) symbObject.put("defaultPatchHeight", defaultPatchHeight);
        if (defaultPatchWidth != null) symbObject.put("defaultPatchWidth", defaultPatchWidth);
        if (groupGap != null) symbObject.put("groupGap", groupGap);
        if (headingGap != null) symbObject.put("headingGap", headingGap);
        if (horizontalItemGap != null) symbObject.put("horizontalItemGap", horizontalItemGap);
        if (horizontalPatchGap != null) symbObject.put("horizontalPatchGap", horizontalPatchGap);
        if (layerNameGap != null) symbObject.put("layerNameGap", layerNameGap);
        if (verticalItemGap != null) symbObject.put("verticalItemGap", verticalItemGap);
        if (verticalPatchGap != null) symbObject.put("verticalPatchGap", verticalPatchGap);
        return symbObject;
    }

    public AgsJsonTextSymbol getTitleSymbol() {
        return titleSymbol;
    }

    public void setTitleSymbol(AgsJsonTextSymbol titleSymbol) {
        this.titleSymbol = titleSymbol;
    }

    public Boolean getShowTitle() {
        return showTitle;
    }

    public void setShowTitle(Boolean showTitle) {
        this.showTitle = showTitle;
    }

    public Double getTitleGab() {
        return titleGab;
    }

    public void setTitleGab(Double titleGab) {
        this.titleGab = titleGab;
    }

    public Double getTextGab() {
        return textGab;
    }

    public void setTextGab(Double textGab) {
        this.textGab = textGab;
    }

    public Double getDefaultPatchHeight() {
        return defaultPatchHeight;
    }

    public void setDefaultPatchHeight(Double defaultPatchHeight) {
        this.defaultPatchHeight = defaultPatchHeight;
    }

    public Double getDefaultPatchWidth() {
        return defaultPatchWidth;
    }

    public void setDefaultPatchWidth(Double defaultPatchWidth) {
        this.defaultPatchWidth = defaultPatchWidth;
    }

    public Double getGroupGap() {
        return groupGap;
    }

    public void setGroupGap(Double groupGap) {
        this.groupGap = groupGap;
    }

    public Double getHeadingGap() {
        return headingGap;
    }

    public void setHeadingGap(Double headingGap) {
        this.headingGap = headingGap;
    }

    public Double getHorizontalItemGap() {
        return horizontalItemGap;
    }

    public void setHorizontalItemGap(Double horizontalItemGap) {
        this.horizontalItemGap = horizontalItemGap;
    }

    public Double getHorizontalPatchGap() {
        return horizontalPatchGap;
    }

    public void setHorizontalPatchGap(Double horizontalPatchGap) {
        this.horizontalPatchGap = horizontalPatchGap;
    }

    public Double getLayerNameGap() {
        return layerNameGap;
    }

    public void setLayerNameGap(Double layerNameGap) {
        this.layerNameGap = layerNameGap;
    }

    public Double getVerticalItemGap() {
        return verticalItemGap;
    }

    public void setVerticalItemGap(Double verticalItemGap) {
        this.verticalItemGap = verticalItemGap;
    }

    public Double getVerticalPatchGap() {
        return verticalPatchGap;
    }

    public void setVerticalPatchGap(Double verticalPatchGap) {
        this.verticalPatchGap = verticalPatchGap;
    }
}
