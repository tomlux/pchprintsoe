/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.*;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.GeometryUtils;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;
import lu.etat.pch.gis.utils.json.graphics.AbstractAgsJsonSymbol;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonPictureMarkerSymbol;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 7, 2010
 * Time: 7:34:28 AM
 */
public class AgsJsonElement {
    private static final String TAG = "AgsJsonElement";
    protected LayoutUtils layoutUtils = new LayoutUtils();
    protected String name;
    protected AgsJsonGeometry geometry;
    protected AbstractAgsJsonSymbol symbol;
    protected Integer anchorPoint;
    protected double width;
    protected double height;
    protected int order = 0;

    public AgsJsonElement(String name, AgsJsonGeometry geometry, AbstractAgsJsonSymbol symbol) {
        this.name = name;
        this.geometry = geometry;
        this.symbol = symbol;
    }

    public AgsJsonElement(SOELogger logger, JSONObject jsonObject) {
        try {
            if (!jsonObject.isNull("name")) this.name = jsonObject.getString("name");
            if (!jsonObject.isNull("geometry"))
                this.geometry = AgsJsonGeometry.convertJsonToAgsJsonGeom(logger, jsonObject.getJSONObject("geometry"));
            if (!jsonObject.isNull("symbol")) {
                this.symbol = AbstractAgsJsonSymbol.convertJsonToAgsJsonSymbol(logger, jsonObject.getJSONObject("symbol"));
            }
            if (!jsonObject.isNull("anchor")) {
                anchorPoint = convertAnchorTextToEsriInt(jsonObject.getString("anchor"));
            }
            if (!jsonObject.isNull("height")) {
                try {
                    this.height = Double.parseDouble(jsonObject.getString("height"));
                } catch (NumberFormatException nfEx) {
                }
            }
            if (!jsonObject.isNull("width")) {
                try {
                    this.width = Double.parseDouble(jsonObject.getString("width"));
                } catch (NumberFormatException nfEx) {
                }

            }
            if (!jsonObject.isNull("order")) this.order = jsonObject.getInt("order");
        } catch (Exception ex) {
            logger.error(TAG, "AgsJsonElement(JSONObject).exception", ex);
        }
    }

    public AgsJsonElement() {
    }

    private static int convertAnchorTextToEsriInt(String anchorTxt) {
        if (anchorTxt.equals("bottomleft"))
            return esriAnchorPointEnum.esriBottomLeftCorner;
        else if (anchorTxt.equals("bottommid"))
            return esriAnchorPointEnum.esriBottomMidPoint;
        else if (anchorTxt.equals("bottomright"))
            return esriAnchorPointEnum.esriBottomRightCorner;
        else if (anchorTxt.equals("midleft"))
            return esriAnchorPointEnum.esriLeftMidPoint;
        else if (anchorTxt.equals("midmid"))
            return esriAnchorPointEnum.esriCenterPoint;
        else if (anchorTxt.equals("midright"))
            return esriAnchorPointEnum.esriRightMidPoint;
        else if (anchorTxt.equals("topleft"))
            return esriAnchorPointEnum.esriTopLeftCorner;
        else if (anchorTxt.equals("topmid"))
            return esriAnchorPointEnum.esriTopMidPoint;
        else if (anchorTxt.equals("topright"))
            return esriAnchorPointEnum.esriTopRightCorner;
        else
            return esriAnchorPointEnum.esriBottomLeftCorner;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("name", name);
        symbObject.put("geometry", geometry.toJSON());
        if (symbol != null) symbObject.put("symbol", symbol.toJSON());
        return symbObject;
    }

    public List<IElement> toArcObject(SOELogger logger) throws IOException {
        List<IElement> retELements = new ArrayList<IElement>();
        IGeometry geom = geometry.toArcObject();
        if (width > 0 && height > 0 && (geometry instanceof IPoint)) {
            IPoint pt = (IPoint) geometry;
            Envelope env = new Envelope();
            try {
                env.putCoords(pt.getX(), pt.getY(), pt.getX() + width, pt.getY() + height);
                geom = env;
            } catch (NumberFormatException e) {
                logger.error(TAG, "AgsJsonElement.toArcObject.geometry.width<>height NumberFormatException: " + width + "<>" + height);
            }
        }
        if (symbol != null) {
            for (ISymbol sym : symbol.toArcObject()) {
                IElement symbolElement = null;
                if (sym instanceof IMarkerSymbol) {
                    MarkerElement markerElement = new MarkerElement();
                    markerElement.setSymbol((IMarkerSymbol) sym);
                    if (geom instanceof IPoint)
                        markerElement.setGeometry(geom);
                    symbolElement = markerElement;
                } else if (sym instanceof ILineSymbol) {
                    LineElement lineElement = new LineElement();
                    if (geom instanceof IPolyline)
                        lineElement.setGeometry(geom);
                    lineElement.setSymbol((ILineSymbol) sym);
                    symbolElement = lineElement;
                } else if (sym instanceof IFillSymbol) {
                    PolygonElement polygonElement = new PolygonElement();
                    polygonElement.setSymbol((IFillSymbol) sym);
                    if (geom instanceof Envelope)
                        polygonElement.setGeometry(GeometryUtils.convertEnvelope2Polygon((Envelope) geom));
                    else if (geom instanceof Polygon)
                        polygonElement.setGeometry(geom);
                    else
                        logger.error(TAG, "AgsJsonElement.toArcObject.IFillSymbol.wrong geometry for: " + this.toJSON());
                    symbolElement = polygonElement;
                } else if (sym instanceof ITextSymbol) {
                    ITextSymbol textSymbol = (ITextSymbol) sym;
                    ITextElement textElement = null;
                    if (geom instanceof Envelope || geom instanceof Polygon) {
                        textElement = new ParagraphTextElement();
                    } else if (geom instanceof Point) {
                        textElement = new TextElement();
                    } else {
                        logger.error(TAG, "AgsJsonElement.toArcObject.ITextSymbol.wrong geometry for: " + this.toJSON());
                    }
                    if (textElement != null) {
                        textElement.setText(textSymbol.getText());
                        ((IElement) textElement).setGeometry(geom);
                        if (anchorPoint == null)
                            anchorPoint = esriAnchorPointEnum.esriCenterPoint;
                        layoutUtils.positionTextElement(textElement, geom, anchorPoint);
                        textElement.setText(textSymbol.getText());
                        textElement.setSymbol(textSymbol);
                        symbolElement = (IElement) textElement;
                    }
                } else if (symbol instanceof AgsJsonPictureMarkerSymbol) {
                    IElement markerElement = ((AgsJsonPictureMarkerSymbol) symbol).toArcObjectsElement(geom, 92);
                    symbolElement = markerElement;
                } else {
                    logger.error(TAG, "AgsJsonElement.toArcObjects.UNKNOWN symbol: " + sym + " -> " + toJSON().toString());
                }
                IElement element = symbolElement;
                if (element != null) {
                    if (name != null) {
                        if (element instanceof IMapSurround) {
                            ((IMapSurround) element).setName(name);
                        } else if (element instanceof IElementProperties) {
                            ((IElementProperties) element).setName(name);
                        }
                    }
                    retELements.add(element);
                } else {
                    logger.error(TAG, "AgsJsonElement.toArcObject.ERROR.retELement=NULL: " + this.toJSON().toString());
                }
            }
        }
        return retELements;
    }

    public int getAnchorPoint() {
        return anchorPoint;
    }

    public void setAnchorPoint(int anchorPoint) {
        this.anchorPoint = anchorPoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public AgsJsonGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(AgsJsonGeometry geometry) {
        this.geometry = geometry;
    }

    public AbstractAgsJsonSymbol getSymbol() {
        return symbol;
    }

    public void setSymbol(AbstractAgsJsonSymbol symbol) {
        this.symbol = symbol;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
