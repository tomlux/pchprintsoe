/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.*;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;
import lu.etat.pch.gis.utils.json.format.AgsJsonSimpleLineDecorationElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 6:56:20 AM
 */
public class AgsJsonPchCartographicLineSymbol extends AbstractAgsJsonSymbol implements AgsJsonLineSymbol {
    private static final String TAG = "AgsJsonSimpleLineSymbol";
    private static Map<String, Integer> esriLineCapStyleMap = new HashMap<String, Integer>();
    private static Map<String, Integer> esriLineJoinStyleMap = new HashMap<String, Integer>();

    static {
        esriLineCapStyleMap.put("BUTT", esriLineCapStyle.esriLCSButt);
        esriLineCapStyleMap.put("ROUND", esriLineCapStyle.esriLCSRound);
        esriLineCapStyleMap.put("SQUARE", esriLineCapStyle.esriLCSSquare);
        esriLineJoinStyleMap.put("BEVEL", esriLineJoinStyle.esriLJSBevel);
        esriLineJoinStyleMap.put("MITRE", esriLineJoinStyle.esriLJSMitre);
        esriLineJoinStyleMap.put("ROUND", esriLineJoinStyle.esriLJSRound);
    }

    public static final String TYPE = "pchCLS";
    private List<AgsJsonSimpleLineDecorationElement> lineDecorationElements = new ArrayList<AgsJsonSimpleLineDecorationElement>();
    private int capStyle = esriLineCapStyle.esriLCSButt;
    private int joinStyle = esriLineJoinStyle.esriLJSMitre;
    private boolean decorationOnTop;
    private AgsJsonRGBColor color;
    private double width = 1;
    private boolean flip;


    public AgsJsonPchCartographicLineSymbol(SOELogger logger) {
        super(logger);
        this.type = TYPE;
    }

    public AgsJsonPchCartographicLineSymbol(SOELogger logger, AgsJsonRGBColor color, double width) {
        this(logger);
        this.color = color;
        this.width = width;
    }

    public AgsJsonPchCartographicLineSymbol(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        try {
            if (!jsonObject.isNull("color")) {
                color = new AgsJsonRGBColor(logger, jsonObject.get("color"));
            }
            if (!jsonObject.isNull("lineDecorationElements")) {
                Object lineDecoElemObj = jsonObject.get("lineDecorationElements");
                if (lineDecoElemObj instanceof JSONObject)
                    lineDecorationElements.add(new AgsJsonSimpleLineDecorationElement(logger, (JSONObject) lineDecoElemObj));
                else if (lineDecoElemObj instanceof JSONArray) {
                    JSONArray lineDeoElemArray = (JSONArray) lineDecoElemObj;
                    for (int i = 0; i < lineDeoElemArray.length(); i++) {
                        lineDecorationElements.add(new AgsJsonSimpleLineDecorationElement(logger, (JSONObject) lineDeoElemArray.get(i)));
                    }
                }
            }
            if (!jsonObject.isNull("capStyle"))
                capStyle = esriLineCapStyleMap.get(jsonObject.getString("capStyle").toUpperCase());
            if (!jsonObject.isNull("joinStyle"))
                capStyle = esriLineJoinStyleMap.get(jsonObject.getString("joinStyle").toUpperCase());
            if (!jsonObject.isNull("width")) width = jsonObject.getDouble("width");
            if (!jsonObject.isNull("decorationOnTop")) decorationOnTop = jsonObject.getBoolean("decorationOnTop");
            if (!jsonObject.isNull("flip")) flip = jsonObject.getBoolean("flip");
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonSimpleLineSymbol(JSONObject)", e);
        }
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        if (lineDecorationElements.size() > 0) {
            JSONArray lineDecoElemArray = new JSONArray();
            for (int i = 0; i < lineDecorationElements.size(); i++) {
                AgsJsonSimpleLineDecorationElement agsJsonSimpleLineDecorationElement = lineDecorationElements.get(i);
                lineDecoElemArray.put(agsJsonSimpleLineDecorationElement.toJSON());
            }
            symbObject.put("lineDecorationElements", lineDecoElemArray);
        }
        symbObject.put("capStyle", capStyle);
        symbObject.put("joinStyle", joinStyle);
        symbObject.put("decorationOnTop", decorationOnTop);
        if (color != null)
            symbObject.put("color", color.toJSON());
        symbObject.put("width", width);
        symbObject.put("flip", flip);
        return symbObject;
    }

    public List<ISymbol> toArcObject() throws IOException, JSONException {
        List<ISymbol> retList = new ArrayList<ISymbol>();
        CartographicLineSymbol cartographicLineSymbol = new CartographicLineSymbol();
        LineDecoration lineDecoration = new LineDecoration();
        for (AgsJsonSimpleLineDecorationElement agsJsonSimpleLineDecorationElement : lineDecorationElements) {
            lineDecoration.addElement(agsJsonSimpleLineDecorationElement.toArcObject());
        }
        cartographicLineSymbol.setLineDecorationByRef(lineDecoration);

        cartographicLineSymbol.setDecorationOnTop(decorationOnTop);
        cartographicLineSymbol.setCap(capStyle);
        cartographicLineSymbol.setJoin(joinStyle);
        cartographicLineSymbol.setFlip(flip);
        //
        /*
        Template template = new Template();
        //2dots 3spaces 4dots 9spaces
        template.addPatternElement(2, 3);
        template.addPatternElement(4, 9);
        template.setInterval(1); //symbolSize * 3
        cartographicLineSymbol.setTemplateByRef(template);
        */
        if (color != null)
            cartographicLineSymbol.setColor(color.toArcObject());
        if (width > 0)
            cartographicLineSymbol.setWidth(width);
        retList.add(cartographicLineSymbol);
        return retList;
    }

    @Override
    public List<ILineSymbol> toLineArcObject() throws IOException {
        List<ISymbol> symbolList = toArcObject();
        List<ILineSymbol> retList = new ArrayList<ILineSymbol>();
        for (ISymbol iSymbol : symbolList) {
            if (iSymbol instanceof ILineSymbol)
                retList.add((ILineSymbol) iSymbol);
        }
        return retList;
    }
}
