/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;
import lu.etat.pch.gis.utils.json.format.AgsJsonTextFormat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 6, 2010
 * Time: 5:41:59 PM
 */
public class AgsJsonTextSymbol extends AbstractAgsJsonSymbol {
    private static final String TAG = "AgsJsonTextSymbol";
    public static final String TYPE = "esriTS";

    private String text = null;
    private double xOffset = 0, yOffset = 0, angle = 0;
    private AgsJsonRGBColor color;
    private AgsJsonRGBColor backgroundColor;
    private AgsJsonTextFormat textFormat;

    public AgsJsonTextSymbol(SOELogger logger) {
        super(logger);
        this.type = TYPE;
    }

    public AgsJsonTextSymbol(SOELogger logger, String text) {
        this(logger);
        this.text = text;
    }

    public AgsJsonTextSymbol(SOELogger logger, JSONObject symbolObject) {
        this(logger);
        try {
            if (!symbolObject.isNull("textFormat")) {
                JSONObject jsonTextFormat = symbolObject.getJSONObject("textFormat");
                textFormat = new AgsJsonTextFormat(logger, jsonTextFormat);
            }
            if (!symbolObject.isNull("font") && textFormat == null) {
                JSONObject fontObject = symbolObject.getJSONObject("font");
                textFormat = new AgsJsonTextFormat(logger, fontObject);
            }
            if (!symbolObject.isNull("text")) text = (symbolObject.getString("text"));
            if (!symbolObject.isNull("xoffset")) xOffset = (symbolObject.getDouble("xoffset"));
            if (!symbolObject.isNull("yoffset")) yOffset = (symbolObject.getDouble("yoffset"));
            if (!symbolObject.isNull("angle"))
                angle = -(symbolObject.getDouble("angle"));
            if (!symbolObject.isNull("color")) {
                color = new AgsJsonRGBColor(logger, symbolObject.get("color"));
                if (textFormat != null && textFormat.getColor() == null) textFormat.setColor(color);
            }
            if (!symbolObject.isNull("backgroundColor"))
                backgroundColor = new AgsJsonRGBColor(logger, symbolObject.get("backgroundColor"));
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonTextSymbol(JSONObject)", e);
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("text", text);
        if (color != null)
            symbObject.put("color", color.toJSON());
        if (backgroundColor != null)
            symbObject.put("backgroundColor", backgroundColor.toJSON());
        symbObject.put("xOffset", xOffset);
        symbObject.put("yOffset", yOffset);
        symbObject.put("angle", angle);
        if (textFormat != null)
            symbObject.put("textFormat", textFormat.toJSON());
        return symbObject;
    }


    public List<ISymbol> toArcObject() throws IOException {
        List<ISymbol> retList = new ArrayList<ISymbol>();
        TextSymbol textSymbol = new TextSymbol();
        textSymbol = (TextSymbol) toArcObject(textSymbol);
        retList.add(textSymbol);
        return retList;
    }

    public ISymbol toArcObject(TextSymbol textSymbol) throws IOException {
        if (textFormat == null) {
            textFormat = new AgsJsonTextFormat(logger);
        }
        if ((textFormat.getColor() == null || textFormat.getColor().toUINT() == 0)
                && color != null) textSymbol.setColor(color.toArcObject());
        textSymbol = textFormat.toArcObjects(textSymbol);
        if (text != null) textSymbol.setText(text);
        textSymbol.setXOffset(xOffset);
        textSymbol.setYOffset(yOffset);
        textSymbol.setAngle(angle);
        if (backgroundColor != null) {
            LineCallout lineCallout = new LineCallout();
            SimpleFillSymbol simpleFillSymbol = new SimpleFillSymbol();
            simpleFillSymbol.setColor(backgroundColor.toArcObject());
            lineCallout.setBorderByRef(simpleFillSymbol);
            ILineSymbol accentBar = lineCallout.getAccentBar();
            lineCallout.setLeaderLineByRef(null);
            lineCallout.setAccentBarByRef(null);
            lineCallout.setTopMargin(0);
            lineCallout.setBottomMargin(0);
            lineCallout.setLeftMargin(0);
            lineCallout.setRightMargin(0);
            textSymbol.setBackgroundByRef(lineCallout);
        }
        return textSymbol;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getxOffset() {
        return xOffset;
    }

    public void setxOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public double getyOffset() {
        return yOffset;
    }

    public void setyOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public AgsJsonRGBColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(AgsJsonRGBColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public AgsJsonTextFormat getTextFormat() {
        return textFormat;
    }

    public void setTextFormat(AgsJsonTextFormat textFormat) {
        this.textFormat = textFormat;
    }
}
