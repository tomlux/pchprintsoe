/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.CharacterMarkerSymbol;
import com.esri.arcgis.display.IMarkerSymbol;
import com.esri.arcgis.display.ISimpleMarkerSymbol;
import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.support.ms.stdole.StdFont;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 6:02:03 PM
 */
public class AgsJsonPchCharacterMarkerSymbol extends AbstractAgsJsonSymbol implements AgsJsonMarkerSymbol {
    private static final String TAG = "AgsJsonSimpleMarkerSymbol";
    public static final String TYPE = "pchCMS";
    private AgsJsonRGBColor color;
    private double size = 12;
    private double angle = 0;
    private double xOffset = 0;
    private double yOffset = 0;
    private AgsJsonSimpleLineSymbol outline;

    private String fontName = "ESRI Default Marker";
    private Integer characterIndex = 35;

    public AgsJsonPchCharacterMarkerSymbol(SOELogger logger) {
        super(logger);
        this.type = TYPE;
        this.color = new AgsJsonRGBColor(logger, 255, 0, 0);
    }

    public AgsJsonPchCharacterMarkerSymbol(SOELogger logger, AgsJsonRGBColor color, double size) {
        this(logger);
        this.color = color;
        this.size = size;
    }

    public AgsJsonPchCharacterMarkerSymbol(SOELogger logger, AgsJsonRGBColor color, double size, String fontName, Integer characterIndex) {
        super(logger);
        this.color = color;
        this.size = size;
        this.fontName = fontName;
        this.characterIndex = characterIndex;
    }

    public AgsJsonPchCharacterMarkerSymbol(SOELogger logger, JSONObject jsonObject) {
        this(logger);
        try {
            if (!jsonObject.isNull("fontName")) fontName = jsonObject.getString("fontName");
            if (!jsonObject.isNull("characterIndex")) characterIndex = jsonObject.getInt("characterIndex");

            if (!jsonObject.isNull("color")) {
                color = new AgsJsonRGBColor(logger, jsonObject.get("color"));
            }
            if (!jsonObject.isNull("size")) size = jsonObject.getDouble("size");
            if (!jsonObject.isNull("angle")) angle = jsonObject.getDouble("angle");
            if (!jsonObject.isNull("xOffset")) xOffset = jsonObject.getDouble("xOffset");
            if (!jsonObject.isNull("yOffset")) yOffset = jsonObject.getDouble("yOffset");
            if (!jsonObject.isNull("outline"))
                outline = new AgsJsonSimpleLineSymbol(logger, jsonObject.getJSONObject("outline"));
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonSimpleMarkerSymbol(JSONObject)", e);
        }
    }

    public double getxOffset() {
        return xOffset;
    }

    public void setxOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public double getyOffset() {
        return yOffset;
    }

    public void setyOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public Integer getCharacterIndex() {
        return characterIndex;
    }

    public void setCharacterIndex(Integer characterIndex) {
        this.characterIndex = characterIndex;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setXOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public void setYOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public AgsJsonSimpleLineSymbol getOutline() {
        return outline;
    }

    public void setOutline(AgsJsonSimpleLineSymbol outline) {
        this.outline = outline;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("fontName", fontName);
        symbObject.put("characterIndex", characterIndex);
        symbObject.put("color", color.toJSON());
        symbObject.put("size", size);
        symbObject.put("angle", angle);
        symbObject.put("xOffset", xOffset);
        symbObject.put("yOffset", yOffset);
        if (outline != null) {
            symbObject.put("outline", outline);
        }
        return symbObject;
    }

    public List<ISymbol> toArcObject() throws JSONException, IOException {
        List<ISymbol> retList = new ArrayList<ISymbol>();
        CharacterMarkerSymbol markerSymbol = new CharacterMarkerSymbol();
        if (fontName != null) {
            StdFont stdFont = new StdFont();
            stdFont.setName(fontName);
            markerSymbol.setFont(stdFont);
        }
        if (characterIndex != null)
            markerSymbol.setCharacterIndex(characterIndex);
        if (color != null)
            markerSymbol.setColor(color.toArcObject());
        if (size > -1)
            markerSymbol.setSize(size);
        markerSymbol.setAngle(angle);
        if (outline != null) {
            if (markerSymbol instanceof ISimpleMarkerSymbol) {
                ISimpleMarkerSymbol simpleMarkerSymbol = (ISimpleMarkerSymbol) markerSymbol;
                simpleMarkerSymbol.setOutline(true);
                simpleMarkerSymbol.setOutlineColor(outline.getColor().toArcObject());
                simpleMarkerSymbol.setOutlineSize(outline.getWidth());
            }
        }
        /*
        if (alpha>=0) {
            IColor color = simpleMarkerSymbol.getColor();
            color.setTransparency((byte) (symbolObject.getDouble("alpha") * 255D));
            simpleMarkerSymbol.setColor(color);
        }
        */
        markerSymbol.setXOffset(xOffset);
        markerSymbol.setYOffset(yOffset);
        retList.add(markerSymbol);
        return retList;
    }

    @Override
    public IMarkerSymbol toMarkerArcObject() throws IOException {
        List<ISymbol> symbols = toArcObject();
        if (symbols.size() == 1) {
            ISymbol symbol = symbols.get(0);
            if (symbol instanceof IMarkerSymbol) {
                return (IMarkerSymbol) symbol;
            }
        }
        return null;
    }
}
