/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.*;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.GeometryUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;
import lu.etat.pch.gis.utils.json.layoutElements.AbstractAgsJsonLayoutElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 16/03/12
 * Time: 17:23
 */
public class AgsJsonLayoutElement extends AgsJsonElement {
    private static final String TAG = "AgsJsonLayoutElement";
    private AbstractAgsJsonLayoutElement layoutElement;

    public AgsJsonLayoutElement(String name, AgsJsonGeometry geometry, AbstractAgsJsonLayoutElement layoutElement) {
        this.name = name;
        this.geometry = geometry;
        this.layoutElement = layoutElement;
    }

    public AgsJsonLayoutElement(String name, AgsJsonGeometry geometry, AbstractAgsJsonLayoutElement layoutElement, int anchorPoint) {
        this.name = name;
        this.geometry = geometry;
        this.layoutElement = layoutElement;
        this.anchorPoint = anchorPoint;
    }

    public AgsJsonLayoutElement(SOELogger logger, JSONObject jsonObject) {
        super(logger, jsonObject);
        if (jsonObject.has("layoutElement")) {
            this.layoutElement = AbstractAgsJsonLayoutElement.getFromJson(logger, jsonObject.getJSONObject("layoutElement"));
        }
        if (symbol == null && layoutElement == null && jsonObject.has("symbol")) {
            this.layoutElement = AbstractAgsJsonLayoutElement.getFromJson(logger, jsonObject.getJSONObject("symbol"));
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = super.toJSON();
        if (layoutElement != null) symbObject.put("layoutElement", layoutElement.toJSON());
        return symbObject;
    }

    public List<IElement> toArcObject(SOELogger logger) throws IOException {
        return toArcObject(logger, null, null);
    }

    public List<IElement> toArcObject(SOELogger logger, PageLayout pageLayout, IMapFrame mapFrame) throws IOException {
        IElement retELement = null;
        IGeometry geom = geometry.toArcObject();
        if (width > 0 && height > 0 && (geom instanceof IPoint)) {
            IPoint pt = (IPoint) geom;
            Envelope env = new Envelope();
            try {
                env.putCoords(pt.getX(), pt.getY(), pt.getX() + width, pt.getY() + height);
                geom = env;
            } catch (NumberFormatException e) {
                logger.error(TAG, "toArcObject.geometry.width<>height NumberFormatException: " + width + "<>" + height);
            }
        }
        if (layoutElement != null) {
            IElement layoutElem = layoutElement.toArcObject(pageLayout, mapFrame, geom);
            if (layoutElem == null) {
                logger.error(TAG, "toArcObject().layoutElem->NULL: " + this.toJSON());
            } else {
                boolean validGeom = false;
                IGeometry layoutElemGeom = layoutElem.getGeometry();
                if (layoutElemGeom != null) {
                    if (layoutElemGeom instanceof IPoint) validGeom = true;
                    else if (layoutElemGeom instanceof IPointCollection)
                        validGeom = (((IPointCollection) layoutElemGeom).getPointCount() > 3);
                }

                if (!validGeom) {
                    layoutElem.setGeometry(geom);
                    layoutElemGeom = geom;
                }
                if (layoutElemGeom instanceof IPoint) {
                    logger.debug(TAG, "toArcObject().geom.Point");
                    if (layoutElem instanceof TextElement) {
                        TextElement textElement = (TextElement) layoutElem;
                        layoutUtils.positionTextElement(textElement, layoutElemGeom, anchorPoint);
                    } else if (layoutElem instanceof MapSurroundFrame) {
                        MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) layoutElem;
                        layoutUtils.positionElement(logger, mapSurroundFrame, (IPoint) layoutElemGeom, pageLayout.getScreenDisplay());
                        if (esriAnchorPointEnum.esriBottomLeftCorner != anchorPoint) {
                            layoutUtils.positionElementX(logger, layoutElem, anchorPoint);
                        }
                    } else {
                        logger.error(TAG, "toArcObjects() NO IMapSurroundFrame found !!! " + toJSON().toString());
                    }
                } else {
                    logger.debug(TAG, "toArcObject().geom: " + layoutElemGeom);
                    if (esriAnchorPointEnum.esriBottomLeftCorner != anchorPoint) {
                        layoutUtils.positionElementX(logger, layoutElem, anchorPoint);
                    }
                }
            }
            retELement = layoutElem;
        }
        if (retELement == null && symbol != null) {
            ISymbol sym = symbol.toArcObject().get(0);
            IElement symbolElement = null;
            if (sym instanceof IMarkerSymbol) {
                MarkerElement markerElement = new MarkerElement();
                markerElement.setSymbol((IMarkerSymbol) sym);
                if (geom instanceof IPoint)
                    markerElement.setGeometry(geom);
                symbolElement = markerElement;
            } else if (sym instanceof ILineSymbol) {
                LineElement lineElement = new LineElement();
                if (geom instanceof IPolyline)
                    lineElement.setGeometry(geom);
                lineElement.setSymbol((ILineSymbol) sym);
                symbolElement = lineElement;
            } else if (sym instanceof IFillSymbol) {
                PolygonElement polygonElement = new PolygonElement();
                polygonElement.setSymbol((IFillSymbol) sym);
                if (geom instanceof Envelope)
                    polygonElement.setGeometry(GeometryUtils.convertEnvelope2Polygon((Envelope) geom));
                else if (geom instanceof Polygon)
                    polygonElement.setGeometry(geom);
                else logger.error(TAG, "toArcObject.IFillSymbol.wrong geometry for", this.toJSON());
                symbolElement = polygonElement;
            } else if (sym instanceof ITextSymbol) {
                ITextSymbol textSymbol = (ITextSymbol) sym;
                ITextElement textElement = null;
                if (geom instanceof Envelope || geom instanceof Polygon) {
                    textElement = new ParagraphTextElement();
                } else if (geom instanceof Point) {
                    textElement = new TextElement();
                } else {
                    logger.error(TAG, "toArcObject.ITextSymbol.wrong geometry for", this.toJSON());
                }
                if (textElement != null) {
                    textElement.setText(textSymbol.getText());
                    ((IElement) textElement).setGeometry(geom);
                    if (pageLayout == null && anchorPoint == null)
                        anchorPoint = esriAnchorPointEnum.esriCenterPoint;
                    layoutUtils.positionTextElement(textElement, geom, anchorPoint);
                    textElement.setText(textSymbol.getText());
                    textElement.setSymbol(textSymbol);
                    symbolElement = (IElement) textElement;
                }
            } else {
                logger.error(TAG, "toArcObjects.UNKNOWN symbol: " + sym + " -> " + toJSON().toString());
            }
            retELement = symbolElement;
            /*todo: test AgsJsonPictureMarkerSymbol
            /*todo: test AgsJsonPictureMarkerSymbol
            IElement markerElement = new AgsJsonPictureMarkerSymbol(logger, graphicSymbolObj).toArcObjectsElement(geom, resolution);
            retElement = markerElement;
            */

        }
        if (retELement != null) {
            if (name != null && !name.equalsIgnoreCase("null")) {
                if (retELement instanceof IMapSurround) {
                    ((IMapSurround) retELement).setName(name);
                } else if (retELement instanceof IElementProperties) {
                    ((IElementProperties) retELement).setName(name);
                }
            }
        } else {
            logger.error(TAG, "toArcObject.ERROR.retELement=NULL", this.toJSON().toString());
        }
        List<IElement> retElements = new ArrayList<IElement>();
        retElements.add(retELement);
        return retElements;
    }

    public AbstractAgsJsonLayoutElement getLayoutElement() {
        return layoutElement;
    }

    public void setLayoutElement(AbstractAgsJsonLayoutElement layoutElement) {
        this.layoutElement = layoutElement;
    }
}
