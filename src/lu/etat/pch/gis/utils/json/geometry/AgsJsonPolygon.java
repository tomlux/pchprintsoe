/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geodatabase.JSONDeserializerGdb;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.JSONReader;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:21:19 PM
 */
public class AgsJsonPolygon extends AgsJsonGeometry {
    private static final String TAG = "AgsJsonPolygon";
    private Polygon polygon;
    private List<AgsJsonRing> rings = new ArrayList<AgsJsonRing>();

    public AgsJsonPolygon(SOELogger logger) {
        super(logger);
    }

    public AgsJsonPolygon(SOELogger logger, JSONObject json) {
        super(logger);
        if (json != null) {
            if (!json.isNull("rings")) {
                try {
                    JSONReader reader = new JSONReader();
                    reader.readFromString(json.toString());
                    JSONDeserializerGdb ds = new JSONDeserializerGdb();
                    ds.initDeserializer(reader, null);
                    polygon = (Polygon) ds.readGeometry(esriGeometryType.esriGeometryPolygon);
                } catch (AutomationException e) {
                    logger.error(TAG, "AgsJsonPolygon(JSONObject).AutomationException", e);
                } catch (UnknownHostException e) {
                    logger.error(TAG, "AgsJsonPolygon(JSONObject).UnknownHostException", e);
                } catch (IOException e) {
                    logger.error(TAG, "AgsJsonPolygon(JSONObject).IOException", e);
                } catch (Exception e) {
                    logger.error(TAG, "AgsJsonPolygon(JSONObject).Exception", e);
                }
            } else {
                throw new JSONException("Input JSON does not contain rings information");
            }
        } else {
            throw new JSONException("JSON is null");
        }
    }

    private void geom2json(Polygon polygon) throws Exception {
        rings.clear();
        if (!polygon.isSimple()) {
            //todo: support for rings for non-simple polygon
            for (int i = 0; i < polygon.getPointCount(); i++) {
                IPoint pt = polygon.getPoint(i);
                addPoint(0, pt.getX(), pt.getY());
            }
        } else {
            int ringIndex = -1;
            try {
                IGeometryBag exteriorRingGeometryBag = polygon.getExteriorRingBag();
                IGeometryCollection exteriorRingGeometryCollection = (IGeometryCollection) exteriorRingGeometryBag;
                System.out.println("polygon.ExteriorRingCount = " + exteriorRingGeometryCollection.getGeometryCount());

                for (int i = 0; i < exteriorRingGeometryCollection.getGeometryCount(); i++) {
                    //System.out.println("polygon.ExteriorRing[" + i + "]");
                    IGeometry exteriorRingGeometry = exteriorRingGeometryCollection.getGeometry(i);
                    IPointCollection exteriorRingPointCollection = (IPointCollection) exteriorRingGeometry;
                    ringIndex++;
                    for (int j = 0; j < exteriorRingPointCollection.getPointCount(); j++) {
                        //System.out.println("Point[" + j + "] = " + PointToString(exteriorRingPointCollection.getPoint(j)));
                        addPoint(ringIndex, exteriorRingPointCollection.getPoint(j).getX(), exteriorRingPointCollection.getPoint(j).getY());
                    }

                    IGeometryBag interiorRingGeometryBag = polygon.getInteriorRingBag((IRing) exteriorRingGeometry);
                    IGeometryCollection interiorRingGeometryCollection = (IGeometryCollection) interiorRingGeometryBag;
                    //System.out.println("polygon.InteriorRingCount[exteriorRing" + i + "] = " + interiorRingGeometryCollection.getGeometryCount());
                    for (int k = 0; k < interiorRingGeometryCollection.getGeometryCount(); k++) {
                        //System.out.println("polygon.InteriorRing[" + k + "]");
                        IGeometry interiorRingGeometry = interiorRingGeometryCollection.getGeometry(k);
                        IPointCollection interiorRingPointCollection = (IPointCollection) interiorRingGeometry;

                        ringIndex++;
                        for (int m = 0; m < interiorRingPointCollection.getPointCount(); m++) {
                            //System.out.println("Point[" + m + "] = " + PointToString(interiorRingPointCollection.getPoint(m)));
                            addPoint(ringIndex, interiorRingPointCollection.getPoint(m).getX(), interiorRingPointCollection.getPoint(m).getY());
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error(TAG, "geom2json.Exception", ex);
            }
        }
    }

    public void addPoint(int ringIndex, double x, double y) throws Exception {
        AgsJsonPoint agsJsonPoint = new AgsJsonPoint(logger, x, y);
        if (ringIndex < rings.size()) {
            rings.get(ringIndex).addPoint(agsJsonPoint);
        } else if (ringIndex == rings.size()) {
            //add a new ring
            AgsJsonRing jsonRing = new AgsJsonRing();
            jsonRing.addPoint(agsJsonPoint);
            rings.add(jsonRing);
        } else {
            throw new Exception("invalid ringIndex");
        }
    }


    private class AgsJsonRing {
        List<AgsJsonPoint> pointAgses;

        private AgsJsonRing() {
            pointAgses = new ArrayList<AgsJsonPoint>();
        }

        public void addPoint(AgsJsonPoint agsJsonPoint) {
            pointAgses.add(agsJsonPoint);
        }

        public List<AgsJsonPoint> getPoints() {
            return pointAgses;
        }

        public int getPointCount() {
            return pointAgses.size();
        }
    }

    public JSONObject toJSON() throws JSONException {
        try {
            geom2json(polygon);
            JSONObject graphics2Geom = new JSONObject();
            JSONArray ringArray = new JSONArray();
            for (AgsJsonRing AgsJsonRing : rings) {
                if (AgsJsonRing.getPointCount() > 1) {
                    JSONArray pointsArray = new JSONArray();
                    for (AgsJsonPoint agsJsonPoint : AgsJsonRing.getPoints()) {
                        JSONArray coordArray = new JSONArray();
                        coordArray.put(agsJsonPoint.getX());
                        coordArray.put(agsJsonPoint.getY());
                        pointsArray.put(coordArray);
                    }
                    pointsArray.put(pointsArray.get(0));
                    ringArray.put(pointsArray);
                }
            }
            graphics2Geom.put("rings", ringArray);
            return graphics2Geom;
        } catch (Exception ex) {
            logger.error(TAG, "toJson.Exception", ex);
        }
        return null;
    }

    public IPolygon toArcObject() throws IOException {
        if (polygon != null) return polygon;
        Polygon polygon = new Polygon();
        for (AgsJsonRing agsJsonRing : rings) {
            List<AgsJsonPoint> ringPoints = agsJsonRing.getPoints();
            for (AgsJsonPoint agsJsonPoint : ringPoints) {
                polygon.addPoint((IPoint) agsJsonPoint.toArcObject(), null, null);
            }
        }
        return polygon;
    }

}
