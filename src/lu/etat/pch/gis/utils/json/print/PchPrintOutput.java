/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.print;

import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.esriUnits;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.UnitConverter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 9, 2010
 * Time: 5:21:26 PM
 */
public class PchPrintOutput {
    private static final String TAG = "PchPrintOuput";
    private SOELogger logger;
    private String pageUnits = "cm";
    private double height = 1, width = 1, resolution = 1;
    private double mapRotation = 0;
    private String format = "pdf";
    private double[] borderWidth = {0, 0, 0, 0};       //{left,right,top,bottom}
    private List<String> toRemoveLayoutElements = new ArrayList<String>();
    private AbstractPchPrintExportSettings exportSettings;
    private Double scale;
    private double referenceScale = -1;
    private String refererWebsite; //holds the ViewerWebPage, needed by token-security to have a valid referer

    public PchPrintOutput(SOELogger logger, String pageUnits, double height, double width, double resolution, double mapRotation, String format) {
        this.logger = logger;
        this.pageUnits = pageUnits;
        this.height = height;
        this.width = width;
        this.resolution = resolution;
        this.mapRotation = mapRotation;
        this.format = format;
    }

    public PchPrintOutput(SOELogger logger, JSONObject jsonObj) throws JSONException {
        this.logger = logger;
        if (!jsonObj.isNull("pageUnits")) pageUnits = jsonObj.getString("pageUnits");
        if (!jsonObj.isNull("height")) height = jsonObj.getDouble("height");
        if (!jsonObj.isNull("width")) width = jsonObj.getDouble("width");
        if (!jsonObj.isNull("resolution")) resolution = jsonObj.getDouble("resolution");
        if (!jsonObj.isNull("scale")) scale = jsonObj.getDouble("scale");
        if (!jsonObj.isNull("referenceScale")) referenceScale = jsonObj.getDouble("referenceScale");
        if (!jsonObj.isNull("refererWebsite")) refererWebsite = jsonObj.getString("refererWebsite");

        if (!jsonObj.isNull("mapRotation"))
            mapRotation = jsonObj.getDouble("mapRotation");
        if (!jsonObj.isNull("format")) format = jsonObj.getString("format");
        if (!jsonObj.isNull("borderWidth")) {
            //JSONArray jsonArray = jsonObj.getJSONArray("borderWidth");
            Object obj = jsonObj.get("borderWidth");
            if (obj instanceof double[]) {
                double[] tmpArray = (double[]) obj;
                if (tmpArray.length == 4) {
                    borderWidth = tmpArray;
                }
            } else if (obj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) obj;
                if (jsonArray.length() == 4) {
                    borderWidth = new double[4];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        borderWidth[i] = jsonArray.getDouble(i);
                    }
                }
            }
        }
        if (!jsonObj.isNull("toRemoveLayoutElements")) {
            toRemoveLayoutElements.clear();
            JSONArray toRemoveLayoutELementsJsonArray = jsonObj.getJSONArray("toRemoveLayoutElements");
            for (int i = 0; i < toRemoveLayoutELementsJsonArray.length(); i++) {
                toRemoveLayoutElements.add(toRemoveLayoutELementsJsonArray.getString(i));
            }
        }

        if (!jsonObj.isNull("exportSettings")) {
            if (format.equals("pdf"))
                exportSettings = new PchPrintExportPDF(jsonObj.getJSONObject("exportSettings"));
        }

    }

    public Double getScale() {
        return scale;
    }

    public String getPageUnits() {
        return pageUnits;
    }

    public void setPageUnits(String pageUnits) {
        this.pageUnits = pageUnits;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public double getMapRotation() {
        return mapRotation;
    }

    public void setMapRotation(double mapRotation) {
        this.mapRotation = mapRotation;
    }


    public void setFormat(String format) {
        this.format = format;
    }

    public double getResolution() {
        return resolution;
    }

    public double getWidth() {
        return width;
    }

    public String getFormat() {
        return format;
    }

    // left,right,top,bottom
    public double[] getBorderWidth() {
        return borderWidth;
    }

    public double getBorderLeft() {
        return borderWidth[0];
    }

    public double getBorderRight() {
        return borderWidth[1];
    }

    public double getBorderTop() {
        return borderWidth[2];
    }

    public double getBorderBottom() {
        return borderWidth[3];
    }

    public List<String> getToRemoveLayoutElements() {
        return toRemoveLayoutElements;
    }

    public void setToRemoveLayoutElements(List<String> toRemoveLayoutElements) {
        this.toRemoveLayoutElements = toRemoveLayoutElements;
    }

    public void setBorderWidth(double[] borderWidth) {
        this.borderWidth = borderWidth;
    }

    public AbstractPchPrintExportSettings getExportSettings() {
        return exportSettings;
    }

    public void setExportSettings(AbstractPchPrintExportSettings exportSettings) {
        this.exportSettings = exportSettings;
    }

    public double getReferenceScale() {
        return referenceScale;
    }

    public void setReferenceScale(double referenceScale) {
        this.referenceScale = referenceScale;
    }

    public String getRefererWebsite() {
        return refererWebsite;
    }

    public void setRefererWebsite(String refererWebsite) {
        this.refererWebsite = refererWebsite;
    }

    public double calculateScale(IEnvelope mapExtentEnvelope, int mapUnits) {
        try {
            int pageUnitValue = UnitConverter.getEsriUnitFromString(logger, pageUnits);
            double mapElementWidthPU = width - (borderWidth[0] + borderWidth[1]); //pageUnits
            double mapElementWidthMU = UnitConverter.convertValue(logger, pageUnitValue, mapUnits, mapElementWidthPU);

            double mapElementHeightPU = height - (borderWidth[2] + borderWidth[3]); //pageUnits
            double mapElementHeightMU = UnitConverter.convertValue(logger, pageUnitValue, mapUnits, mapElementHeightPU);

            double mapExtentWidthMU = mapExtentEnvelope.getWidth();
            double mapExtentHeightMU = mapExtentEnvelope.getHeight();
            double scaleW = mapExtentWidthMU / mapElementWidthMU;
            double scaleH = mapExtentHeightMU / mapElementHeightMU;

            logger.debug(TAG, "calculateScale.scaleW", scaleW);
            logger.debug(TAG, "calculateScale.scaleH", scaleH);

            double retScale = Math.max(scaleH, scaleW);
            logger.debug(TAG, "calculateScale.retScale", retScale);
            return retScale;
        } catch (IOException e) {
            logger.error(TAG, "calculateScale.IOException", e);
        }
        return -1;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("pageUnits", pageUnits);
        jsonObj.put("height", height);
        jsonObj.put("width", width);
        jsonObj.put("resolution", resolution);
        jsonObj.put("mapRotation", mapRotation);
        jsonObj.put("format", format);
        if (scale != null) jsonObj.put("scale", scale);
        jsonObj.put("referenceScale", referenceScale);
        jsonObj.put("borderWidth", Arrays.asList(borderWidth).get(0));
        jsonObj.put("refererWebsite", refererWebsite);
        if (toRemoveLayoutElements.size() > 0) {
            JSONArray array = new JSONArray();
            for (String toRemoveLayoutElement : toRemoveLayoutElements) {
                array.put(toRemoveLayoutElement);
            }
            jsonObj.put("toRemoveLayoutElements", array);
        }
        if (exportSettings != null) {
            jsonObj.put("exportSettings", exportSettings.toJSON());
        }
        return jsonObj;
    }

    public IEnvelope calculateMapFrameEnvelope() {
        try {
            IEnvelope envelope = new Envelope();
            envelope.putCoords(getBorderLeft(), getBorderBottom(), getWidth() - getBorderRight(), getHeight() - getBorderTop());
            return envelope;
        } catch (IOException e) {
            logger.error(TAG, "calculateMapFrameEnvelope", e);
        }
        return null;
    }

    public double getHeightInPixel() {
        double value = UnitConverter.convertValue(logger, UnitConverter.getEsriUnitFromString(logger, pageUnits), esriUnits.esriInches, height) * resolution;
        logger.debug(TAG, "getHeightInPixel", value);
        return value;
    }

    public double getWidthInPixel() {
        double value = UnitConverter.convertValue(logger, UnitConverter.getEsriUnitFromString(logger, pageUnits), esriUnits.esriInches, width) * resolution;
        logger.debug(TAG, "getWidthInPixel", value);
        return value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PchPrintOutput");
        sb.append(" pageUnits='").append(pageUnits).append('\'');
        sb.append(", height=").append(height);
        sb.append(", width=").append(width);
        sb.append(", resolution=").append(resolution);
        sb.append(", mapRotation=").append(mapRotation);
        sb.append(", format='").append(format).append('\'');
        sb.append(", borderWidth=").append(borderWidth == null ? "null" : "");
        for (int i = 0; borderWidth != null && i < borderWidth.length; ++i)
            sb.append(i == 0 ? "" : ", ").append(borderWidth[i]);
        sb.append(", toRemoveLayoutElements=").append(toRemoveLayoutElements);
        sb.append(", exportSettings=").append(exportSettings);
        sb.append(", scale=").append(scale);
        sb.append(", referenceScale=").append(referenceScale);
        sb.append(", refererWebsite='").append(refererWebsite).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
