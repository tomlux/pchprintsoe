/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.print;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.geometry.IProjectedCoordinateSystem;
import com.esri.arcgis.geometry.ISpatialReference;
import com.esri.arcgis.geometry.SpatialReferenceEnvironment;
import com.esri.arcgis.geometry.esriSRProjCS4Type;
import com.esri.arcgis.gisclient.*;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.server.json.JSONTokener;
import com.esri.arcgis.system.IName;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.PropertySet;
import lu.etat.pch.gis.utils.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * User: schullto
 * Date: 12/11/2014
 * Time: 5:06 PM
 */
public class PchPrintMapServiceHelper {
    private static final String TAG = PchPrintMapServiceHelper.class.getName();
    private PropertiesFileHelper propertiesFileHelper = new PropertiesFileHelper();
    private DataConnectUtils dataConnectUtils = new DataConnectUtils();
    private LayerUtils layerUtils = new LayerUtils();

    public List<PchPrintMapService> parseJsonArray(SOELogger logger, JSONArray mapServiceArray) {
        List<PchPrintMapService> printMapServices = new ArrayList<PchPrintMapService>();
        try {
            for (int i = 0; i < mapServiceArray.length(); i++)
                printMapServices.add(new PchPrintMapService(mapServiceArray.getJSONObject(i)));
        } catch (JSONException e) {
            logger.error(TAG, "PchPrintMapService.parseJsonArray", e);
        }
        return printMapServices;
    }

    public List<ILayer> createLayersFromPchPrintMapServices(SOELogger logger, List<PchPrintMapService> mapServices, String refererWebsite, ISpatialReference spatialReference, String authorization401) {
        logger.debug(TAG, "createLayersFromPchPrintMapServices.mapServices.size", mapServices.size());
        List<ILayer> layerList = new ArrayList<ILayer>();
        for (PchPrintMapService mapService : mapServices) {
            logger.debug(TAG, "createLayersFromPchPrintMapServices.mapService", mapService);
            try {
                logger.debug(TAG, "createLayersFromPchPrintMapServices.mapService.url", mapService.getUrl());
                double restAlpha = mapService.getAlpha().doubleValue();
                short agsAlpha = (short) ((1 - restAlpha) * 100);
                logger.debug(TAG, "createLayersFromPchPrintMapServices.agsAlpha", agsAlpha);
                logger.debug(TAG, "createLayersFromPchPrintMapServices.mapService.type", mapService.getType());
                ILayer layer = getLayerForMapService(logger, mapService, agsAlpha, refererWebsite, spatialReference, authorization401);
                logger.debug(TAG, "createLayersFromPchPrintMapServices.layer", layer);
                if (layer != null) {
                    logger.debug(TAG, "createLayersFromPchPrintMapServices.layer.name", layer);
                    logger.debug(TAG, "createLayersFromPchPrintMapServices.layer.isValid", layer.isValid());
                    layerList.add(layer);
                    logger.debug(TAG, "createLayersFromPchPrintMapServices.layerAdded", layer);
                } else
                    logger.error(TAG, "createLayersFromPchPrintMapServices.unableToAddMapserviceLayersFor :" + mapService.getUrl());
            } catch (Exception e) {
                logger.error(TAG, "createLayersFromPchPrintMapServices.Exception", e);
            }
        }
        logger.debug(TAG, "createLayersFromPchPrintMapServices.layerList.size: " + layerList.size());
        return layerList;
    }

    public ILayer getLayerForMapService(SOELogger logger, PchPrintMapService mapService, short agsAlpha, String refererWebsite, ISpatialReference spatialReference, String authorization401) {
        logger.debug(TAG, "getLayerForMapService...");
        if (mapService.getUrl() == null) {
            logger.error(TAG, "PchPrintMapService.getLayerForMapService.layer.URL=null!! " + mapService.toJSON().toString());
            return null;
        }

        String replacementLyrFile = getLyrFileReplacement(logger, mapService);
        ILayer layer;
        if (replacementLyrFile != null) {
            layer = addLyrLayer(logger, replacementLyrFile, null, agsAlpha, refererWebsite, authorization401);
        } else if ("LYR".equalsIgnoreCase(mapService.getType())) {
            layer = addLyrLayer(logger, mapService.getUrl(), mapService.getVisibleIds(), agsAlpha, refererWebsite, authorization401);
        } else if ("WMS".equalsIgnoreCase(mapService.getType())) {
            layer = addWMSLayer(logger, mapService, agsAlpha, spatialReference);
        } else if ("WMTS".equalsIgnoreCase(mapService.getType())) {
            layer = addWMTSLayer(logger, mapService, agsAlpha, spatialReference);
        } else {
            ILayer toAddLayer = null;
            if (!mapService.getUrl().toLowerCase().contains(".arcgisonline.com/")) {
                logger.debug(TAG, "getLayerForMapService.addPChPrintSOEMapServiceLayer...");
                toAddLayer = addPChPrintSOEMapServiceLayer(logger, agsAlpha, mapService, refererWebsite, authorization401);
                logger.debug(TAG, "getLayerForMapService.addPChPrintSOEMapServiceLayer", toAddLayer);
            }
            if (toAddLayer == null) {
                logger.debug(TAG, "getLayerForMapService.addAGSMapServiceByLayerFile...");
                toAddLayer = addAGSMapServiceByLayerFile(logger, agsAlpha, mapService, refererWebsite, authorization401);
                logger.debug(TAG, "getLayerForMapService.addAGSMapServiceByLayerFile", toAddLayer);
            }

            if (toAddLayer == null) {
                logger.debug(TAG, "getLayerForMapService.addAGSInternetLayer...");
                toAddLayer = addAGSInternetLayer(logger, agsAlpha, mapService);
                logger.debug(TAG, "getLayerForMapService.addAGSInternetLayer", toAddLayer);
            }
            layer = toAddLayer;
        }
        logger.debug(TAG, "getLayerForMapService.layer: " + layer);
        if (layer == null) {
            logger.error(TAG, "PchPrintMapService.getLayerForMapService.IS-NULL: " + mapService.toJSON().toString());
        }
        return layer;
    }

    private ILayer addLyrLayer(SOELogger logger, String lyrUrl, String visibleIds, short agsAlpha, String refererWebsite, String authorization401) {
        logger.debug(TAG, "addLyrLayer...");
        try {
            logger.debug(TAG, "addLyrLayer.lyrUrl", lyrUrl);
            File agsTmpFile = null;
            boolean canDeleteFile = false;
            try {
                URI uri = new URI(lyrUrl);
                logger.debug(TAG, "addLyrLayer.validURI", uri);
                agsTmpFile = new File(uri);
                logger.debug(TAG, "addLyrLayer.validURI.agsTmpFile", agsTmpFile);
                if (agsTmpFile.canRead()) {
                    canDeleteFile = false;
                } else {
                    agsTmpFile = null;
                    logger.debug(TAG, "addLyrLayer.validURI.cannotReadFile.agsTmpFile:", agsTmpFile);
                }
            } catch (URISyntaxException ignored) {
            }
            if (agsTmpFile == null) {
                agsTmpFile = File.createTempFile("LyrLayer", ".lyr");
                logger.debug(TAG, "addLyrLayer.agsTmpFile", agsTmpFile);
                RESTUtils.downloadFile(logger, lyrUrl, agsTmpFile, refererWebsite, authorization401);
                /*
                if (!RESTUtils.urlExists(logger, lyrUrl, refererWebsite, authorization401)) {
                    logger.debug(TAG, "addLyrLayer.urlNotFound", lyrUrl);
                    return null;
                }
                */
                canDeleteFile = true;
            }
            String lyrFileName = agsTmpFile.getAbsolutePath();
            logger.debug(TAG, "addLyrLayer.lyrFileName", lyrFileName);
            ILayer layer = null;
            LayerFile layerFile = new LayerFile();
            if (lyrFileName != null && layerFile.isLayerFile(lyrFileName)) {
                logger.debug(TAG, "addLyrLayer.isLayerFile");
                layerFile.open(lyrFileName);
                layer = layerFile.getLayer();
                layerFile.close();
                layer.setVisible(true);
                logger.debug(TAG, "addLyrLayer.layer", layer);
                //System.out.println("layer = " + layer);
                if (layer instanceof ILayerEffects) {
                    ((ILayerEffects) layer).setTransparency(agsAlpha);
                }
                logger.debug(TAG, "addLyrLayer.mapServiceVisibleIds", visibleIds);
                if (visibleIds != null && layer instanceof MapServerLayer) {
                    configureVisibilityForMapService(logger, (MapServerLayer) layer, visibleIds);
                    logger.debug(TAG, "addLyrLayer.visibilityConfigured");
                } else {
                    logger.debug(TAG, "addLyrLayer.layer.unknownClass", layer);
                }
            } else {
                logger.error(TAG, "Layer not found !!!! : " + lyrUrl);
            }
            if (canDeleteFile) {
                logger.debug(TAG, "addLyrLayer.agsTmpFile.delete", agsTmpFile);
                boolean deleted = agsTmpFile.delete();
                logger.debug(TAG, "addLyrLayer.agsTmpFile.deleted", deleted);
            }
            return layer;
        } catch (IOException e) {
            logger.error(TAG, "addLyrLayer.IOException", e);
        }
        return null;
    }

    private ILayer addAGSMapServiceByLayerFile(SOELogger logger, short agsAlpha, PchPrintMapService mapService, String refererWebsite, String authentication401) {
        try {
            String url = mapService.getUrl();
            if (mapService.getProxyURL() != null) {
                url = mapService.getProxyURL() + "?" + url;
            }
            String agsMapServiceLayerUrl = url.contains(".lyr") ? url :
                    url + "?f=lyr";
//                  url + "?f=lyr&v=9.3";
            String token = mapService.getToken();
            if (token != null && token.length() > 0)
                agsMapServiceLayerUrl = agsMapServiceLayerUrl + "&token=" + token;
            logger.debug(TAG, "addAGSMapServiceByLayerFile.agsMapServiceLayerUrl", agsMapServiceLayerUrl);
            //if (!RESTUtils.urlExists(logger, agsMapServiceLayerUrl)) {
            if (!IOUtils.resourceExists(logger, agsMapServiceLayerUrl, authentication401)) {
                logger.warning(TAG, "addAGSMapServiceByLayerFile.urlNotFound: " + agsMapServiceLayerUrl);
                return null;
            }
            File agsTmpFile = File.createTempFile("AGSLayer", ".lyr");
            logger.debug(TAG, "addAGSMapServiceByLayerFile.agsTmpFile", agsTmpFile);
            IOUtils.getResourceAsFile(logger, agsMapServiceLayerUrl, agsTmpFile, refererWebsite, authentication401);
            String agsTmpFileName = agsTmpFile.getAbsolutePath();
            logger.debug(TAG, "addAGSMapServiceByLayerFile.agsTmpFileName", agsTmpFileName);
            ILayer layer = null;
            LayerFile layerFile = new LayerFile();
            if (layerFile.isLayerFile(agsTmpFileName)) {
                logger.debug(TAG, "addAGSMapServiceByLayerFile.isLayerFile");
                layerFile.open(agsTmpFileName);
                layer = layerFile.getLayer();
                layerFile.close();
                layer.setVisible(true);
                logger.debug(TAG, "addAGSMapServiceByLayerFile.layer", layer);
                if (!layer.isValid()) {
                    logger.debug(TAG, "addAGSMapServiceByLayerFile.LAYER_IS_NOT_VALID", layer);
                    return layer;
                }
                if (layer instanceof ILayerEffects) {
                    ((ILayerEffects) layer).setTransparency(agsAlpha);
                }
                String mapServiceVisibleIds = mapService.getVisibleIds();
                logger.debug(TAG, "addAGSMapServiceByLayerFile.mapServiceVisibleIds", mapServiceVisibleIds);
                if (mapServiceVisibleIds != null && layer instanceof MapServerLayer) {
                    configureVisibilityForMapService(logger, (MapServerLayer) layer, mapServiceVisibleIds);
                    logger.debug(TAG, "addAGSMapServiceByLayerFile.visibilityConfigured");
                } else {
                    logger.debug(TAG, "addAGSMapServiceByLayerFile.layer.class", layer);
                }
                if (layer instanceof MapServerLayer) {
                    MapServerLayer msLayer = (MapServerLayer) layer;
                    String imageFormat = mapService.getImageFormat();
                    if (imageFormat != null) {
                        int imageFormatCode = getImageFormatCode(imageFormat);
                        if (imageFormatCode != esriImageFormat.esriImageNone) {
                            logger.debug(TAG, "addAGSMapServiceByLayerFile.imageFormat.testIfSupported", imageFormat);
                            for (int i = 0; i < msLayer.getSupportedImageFormatCount(); i++) {
                                if (imageFormatCode == msLayer.getSupportedImageFormat(i)) {
                                    logger.debug(TAG, "addAGSMapServiceByLayerFile.imageFormat.supported.settingTo", imageFormat);
                                    msLayer.setRequestedImageType(imageFormatCode);
                                    break;
                                }
                            }
                        }
                    }
                    msLayer.setCached(false);
                }

            } else {
                logger.error(TAG, "Layer not found !!!! : " + agsMapServiceLayerUrl);
            }
            boolean deleted = agsTmpFile.delete();
            logger.debug(TAG, "addAGSMapServiceByLayerFile.agsTmpFile.deleted", deleted);
            return layer;
        } catch (Exception e) {
            logger.error(TAG, "addAGSMapServiceByLayerFile.Exception", e);
        }
        return null;
    }

    private int getImageFormatCode(String imageFormat) {
        if (imageFormat.equalsIgnoreCase("PNG32")) return esriImageFormat.esriImagePNG32;
        else if (imageFormat.equalsIgnoreCase("PNG24")) return esriImageFormat.esriImagePNG24;
        else if (imageFormat.equalsIgnoreCase("PNG")) return esriImageFormat.esriImagePNG;
        else if (imageFormat.equalsIgnoreCase("JPG")) return esriImageFormat.esriImageJPG;
        else if (imageFormat.equalsIgnoreCase("JPEG")) return esriImageFormat.esriImageJPG;
        else if (imageFormat.equalsIgnoreCase("JPGPNG")) return esriImageFormat.esriImageJPGPNG;
        else if (imageFormat.equalsIgnoreCase("DIB")) return esriImageFormat.esriImageDIB;
        else if (imageFormat.equalsIgnoreCase("TIFF")) return esriImageFormat.esriImageTIFF;
        else if (imageFormat.equalsIgnoreCase("EMF")) return esriImageFormat.esriImageEMF;
        else if (imageFormat.equalsIgnoreCase("PS")) return esriImageFormat.esriImagePS;
        else if (imageFormat.equalsIgnoreCase("PDF")) return esriImageFormat.esriImagePDF;
        else if (imageFormat.equalsIgnoreCase("AI")) return esriImageFormat.esriImageAI;
        else if (imageFormat.equalsIgnoreCase("GIF")) return esriImageFormat.esriImageGIF;
        else if (imageFormat.equalsIgnoreCase("BMP")) return esriImageFormat.esriImageBMP;
        else if (imageFormat.equalsIgnoreCase("SVG")) return esriImageFormat.esriImageSVG;
        else if (imageFormat.equalsIgnoreCase("SVGZ")) return esriImageFormat.esriImageSVGZ;
        else return esriImageFormat.esriImageNone;
    }

    private GroupLayer addPChPrintSOEMapServiceLayer(SOELogger logger, short agsAlpha, PchPrintMapService mapService, String refererWebsite, String authorization401) {
        logger.debug(TAG, "addPChPrintSOEMapServiceLayer...");
        try {
            String url = mapService.getUrl();
            if (mapService.getProxyURL() != null) {
                url = mapService.getProxyURL() + "?" + url;
            }
            String token = "";
            if (mapService.getToken() != null && mapService.getToken().trim().length() > 0) {
                token = "&token=" + mapService.getToken().trim();
                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.token", token);
            } else {
                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.NO_token_USAGE");
            }
            String pchPrintSOELayerUrl = url + PchPrintMapService.pchPrintSOEname + "?f=json" + token;
            logger.debug(TAG, "addPChPrintSOEMapServiceLayer.pchPrintSOELayerUrl", pchPrintSOELayerUrl);
            if (!RESTUtils.urlExists(logger, pchPrintSOELayerUrl, refererWebsite, authorization401)) {
                logger.warning(TAG, "addPChPrintSOEMapServiceLayer.urlNotFound: " + pchPrintSOELayerUrl);
                return null;
            }
            logger.debug(TAG, "addPChPrintSOEMapServiceLayer.mapServiceUrl.exists", url);
            GroupLayer mapServiceGroupLayer = new GroupLayer();
            mapServiceGroupLayer.setName(mapService.getName());
            mapServiceGroupLayer.setVisible(true);
            mapServiceGroupLayer.setTransparency(agsAlpha);
            logger.debug(TAG, "addPChPrintSOEMapServiceLayer.visibleId", mapService.getVisibleIds());
            boolean mapServiceValid = true;
            if (mapService.getVisibleIds() != null && mapService.getVisibleIds().trim().length() > 0) {
                if (mapService.getVisibleIds().equals("*")) {
//                    pchPrintSOELayerUrl = mapServiceUrl + pchPrintSOEname + "/layerFile?layerId=" + "layers_ALL" + "&f=json" + token;
                    pchPrintSOELayerUrl = url + PchPrintMapService.pchPrintSOEname + "/layerFile?f=json&layerId=" + "layers_ALL" + token;
                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.pchPrintSOELayerUrl", pchPrintSOELayerUrl);
                    String jsonResp = RESTUtils.downloadContent(logger, pchPrintSOELayerUrl, refererWebsite, authorization401);
                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.jsonResp", jsonResp);
                    try {
                        if (jsonResp.startsWith("{")) {
                            JSONObject jsonObj = new JSONObject(new JSONTokener(jsonResp));
                            if (!jsonObj.isNull("layerFile")) {
                                String layerFileName = jsonObj.getString("layerFile");
                                if (layerFileName.startsWith("/rest/directories/"))
                                    layerFileName = "http://localhost:6080/arcgis" + layerFileName;
                                ILayer layer = downloadAndLoadLyrFile(logger, layerFileName, refererWebsite, authorization401);
                                if (layer != null && layerIsValid(logger, layer)) {
                                    //Add layer to groupLayer
                                    layer.setVisible(true);
                                    mapServiceGroupLayer.add(layer);
                                } else {
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.JSON.Layer Invalid found !!!");
                                    mapServiceValid = false;
                                }
                            } else {
                                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.JSON.No Layer File found !!!");
                                mapServiceValid = false;
                            }
                        } else if (jsonResp.toLowerCase().startsWith("<html><head>")
                                && (jsonResp.toLowerCase().contains("layerfile:") || jsonResp.toLowerCase().contains("<b>layerfile</b>: "))) {
                            logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.jsonResp seems to be HTML format !!!");
                            String tmpLayerFileName = null;
                            if (jsonResp.toLowerCase().contains("layerFile:"))
                                tmpLayerFileName = jsonResp.substring(jsonResp.toLowerCase().indexOf("layerfile:") + 10);
                            if (jsonResp.toLowerCase().contains("\"<b>layerFile</b>:"))
                                tmpLayerFileName = jsonResp.substring(jsonResp.toLowerCase().indexOf("<b>layerfile</b>: ") + 18);
                            if (tmpLayerFileName == null) {
                                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.HTML.no LayerFile info found !!!");
                                mapServiceValid = false;
                            } else {
                                tmpLayerFileName = tmpLayerFileName.substring(0, tmpLayerFileName.indexOf(".lyr") + 4);
                                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.HTML.tmpLayerFileName", tmpLayerFileName);
                                String layerFileName = tmpLayerFileName;
                                layerFileName = layerFileName.substring(0, layerFileName.indexOf(".lyr") + 4);
                                layerFileName = layerFileName.trim();
                                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.HTML.layerFileName", layerFileName);
                                ILayer layer = downloadAndLoadLyrFile(logger, layerFileName, refererWebsite, authorization401);
                                if (layer != null && layerIsValid(logger, layer)) {
                                    //Add layer to groupLayer
                                    if (layer instanceof ILayerGeneralProperties) {
                                        ILayerGeneralProperties layerGeneralProperties = (ILayerGeneralProperties) layer;
                                        layerGeneralProperties.setLayerDescription(url + "#ALL");
                                    }
                                    layer.setVisible(true);
                                    mapServiceGroupLayer.add(layer);
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.HTML.layer ADDED");
                                } else {
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.*.HTML.no Invalid found !!!");
                                    mapServiceValid = false;
                                }
                            }
                        } else {
                            logger.error(TAG, "addPChPrintSOEMapServiceLayer.*.jsonResp IS NOT A VALID JSON-Format !!!");
                            mapServiceValid = false;
                        }
                    } catch (JSONException e) {
                        logger.error(TAG, "addPChPrintSOEMapServiceLayer.*.JSONException", e);
                        mapServiceValid = false;
                    }
                } else {
                    String[] visibleLayerList = mapService.getVisibleIds().split(",");
                    for (int i = 0; i < visibleLayerList.length; i++) {
                        String visibleLayerId = visibleLayerList[i];
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.splittedVisibleLayerId", visibleLayerId);
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.mapServiceUrl", url);
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.pchPrintSOEname", PchPrintMapService.pchPrintSOEname);
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.visibleLayerId", visibleLayerId);
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.token", token);
                        pchPrintSOELayerUrl = url + PchPrintMapService.pchPrintSOEname + "/layerFile?f=json&layerId=" + visibleLayerId + token;
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.pchPrintSOELayerUrl", pchPrintSOELayerUrl);
                        String jsonResp = RESTUtils.downloadContent(logger, pchPrintSOELayerUrl, refererWebsite, authorization401);
                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.jsonResp", jsonResp);
                        try {
                            String layerFileName2 = null;
                            if (jsonResp.startsWith("{")) {
                                JSONObject jsonObj = new JSONObject(new JSONTokener(jsonResp));
                                if (!jsonObj.isNull("layerFile")) {
                                    String tmpLayerFileName = jsonObj.getString("layerFile");
                                    if (tmpLayerFileName.startsWith("/rest/directories/"))
                                        tmpLayerFileName = "http://localhost:6080/arcgis" + tmpLayerFileName;
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.JSON.tmpLayerFileName: " + tmpLayerFileName);
                                    layerFileName2 = tmpLayerFileName;
                                } else {
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.No Layer File found !!!");
                                    mapServiceValid = false;
                                    break;
                                }
                            } else if (jsonResp.toLowerCase().startsWith("<html><head>")
                                    && (jsonResp.toLowerCase().contains("layerfile:") || jsonResp.toLowerCase().contains("<b>layerfile</b>: "))) {
                                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.jsonResp seems to be HTML format !!!");

                                String tmpLayerFileName = null;
                                if (jsonResp.toLowerCase().contains("layerFile:"))
                                    tmpLayerFileName = jsonResp.substring(jsonResp.toLowerCase().indexOf("layerfile:") + 10);
                                if (jsonResp.toLowerCase().contains("\"<b>layerFile</b>:"))
                                    tmpLayerFileName = jsonResp.substring(jsonResp.toLowerCase().indexOf("<b>layerfile</b>: ") + 18);
                                if (tmpLayerFileName != null) {
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.HTML.tmpLayerFileName", tmpLayerFileName);
                                    layerFileName2 = tmpLayerFileName;
                                } else {
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.HTML.no LayerFile info found !!!");
                                    mapServiceValid = false;
                                    break;
                                }
                            } else {
                                logger.error(TAG, "addPChPrintSOEMapServiceLayer.123.htmlResp, NO layerFile info found in HTML-Format !!!");
                                mapServiceValid = false;
                            }

                            if (layerFileName2 != null) {
                                ILayer layer = downloadAndLoadLyrFile(logger, layerFileName2, refererWebsite, authorization401);
                                logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.layer", layer);
                                if (layer != null && layerIsValid(logger, layer)) {
                                    //Add layer to groupLayer
                                    if (layer instanceof ILayerGeneralProperties) {
                                        ILayerGeneralProperties layerGeneralProperties = (ILayerGeneralProperties) layer;
                                        layerGeneralProperties.setLayerDescription(url + "#" + visibleLayerId);
                                    }
                                    if (layer instanceof FeatureLayer) {
                                        try {
                                            IFeatureLayerDefinition fLayer = (IFeatureLayerDefinition) layer;
                                            if (mapService.getDefinitionExpression() != null) {
                                                List<String> definitionExpression = mapService.getDefinitionExpression();
                                                if (i < definitionExpression.size()) {
                                                    String defExp = definitionExpression.get(i);
                                                    if (defExp != null && defExp.trim().length() > 0) {
                                                        logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.definitionQuery: " + defExp);
                                                        fLayer.setDefinitionExpression(defExp);
                                                    }
                                                } else {
                                                    logger.error(TAG, "layerIndex > definitionExpression.size!!! ignoring it...");
                                                }
                                            }
                                        } catch (NumberFormatException ex) {
                                            logger.error(TAG, "addPChPrintSOEMapServiceLayer.123.definitionQuery.NumberFormatException: " + visibleLayerId, ex);
                                        } catch (ArrayIndexOutOfBoundsException ex) {
                                            logger.error(TAG, "addPChPrintSOEMapServiceLayer.123.definitionQuery.ArrayIndexOutOfBoundsException: " + visibleLayerId, ex);
                                        }
                                    }
                                    layer.setVisible(true);
                                    mapServiceGroupLayer.add(layer);
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.layer ADDED");
                                } else {
                                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.123.Layer Invalid found !!!");
                                    mapServiceValid = false;
                                    break;
                                }

                            }
                        } catch (JSONException e) {
                            logger.error(TAG, "addPChPrintSOEMapServiceLayer.123.JSONException", e);
                            mapServiceValid = false;
                        }
                    }
                }
            }
            logger.debug(TAG, "addPChPrintSOEMapServiceLayer.mapServiceValid: " + mapServiceValid);
            if (mapServiceValid) {
                List<String> definitionExpression = mapService.getDefinitionExpression();
                if (definitionExpression != null) {
                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.trying to apply 'definitionExpression'", definitionExpression);
                    if (mapServiceGroupLayer.getCount() == 1) {
                        ILayer layer = mapServiceGroupLayer.getLayer(0);
                        if (layer instanceof FeatureLayer) {
                            FeatureLayer featureLayer = (FeatureLayer) layer;
                            featureLayer.setDefinitionExpression(mapService.getDefinitionExpression().get(0));
                            logger.debug(TAG, "addPChPrintSOEMapServiceLayer.applied 'definitionExpression' to FeatureLayer. FeatureLayer.name", featureLayer.getName());
                        } else {
                            logger.error(TAG, "addPChPrintSOEMapServiceLayer.UNABLE to apply 'definitionExpression' to a non-FeatureLayer! layer", layer);
                        }
                    } else {
                        logger.error(TAG, "addPChPrintSOEMapServiceLayer.UNABLE to apply 'definitionExpression': LayerCount>1!!! count", mapServiceGroupLayer.getCount());
                    }
                } else {
                    logger.debug(TAG, "addPChPrintSOEMapServiceLayer.no 'definitionExpression' defined.");

                }
                return mapServiceGroupLayer;
            }
        } catch (IOException e) {
            logger.error(TAG, "addPChPrintSOEMapServiceLayer.IOException", e);
        }
        return null;
    }

    private ILayer addWMTSLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha, ISpatialReference mapSpatial) {
        logger.debug(TAG, "addWMTSLayer...");
        WMTSLayer wmtsLayer = null;
        String url = mapService.getUrl();
        if (mapService.getProxyURL() != null) {
            url = mapService.getProxyURL() + "?" + url;
        }
        logger.debug(TAG, "addWMTSLayer.url: " + url);
        try {
            wmtsLayer = dataConnectUtils.createWMTSLayer(url, mapService.getVisibleIds(), agsAlpha);
        } catch (IOException e) {
            logger.debug(TAG, "addWMTSLayer.IOException", e.getMessage());
            e.printStackTrace();
        }
        logger.debug(TAG, "addWMTSLayer.wmtsLayer", wmtsLayer);
        return wmtsLayer;
    }

    ///////////////////////////////////////////////
    // Add Wms Layer Michaël Hansroul 21/02/2011 //
    ///////////////////////////////////////////////
    //       Mod by Mike Mauersberger 13/01/2012 //
    ///////////////////////////////////////////////
    private ILayer addWMSLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha, ISpatialReference mapSpatial) {
        WMSMapLayer wmsMapLayer = null;
        logger.debug(TAG, "addWMSLayer.1...");
        String url = mapService.getUrl();
        if (mapService.getProxyURL() != null) {
            url = mapService.getProxyURL() + "?" + url;
        }
        logger.debug(TAG, "addWMSLayer.url: " + url);
        try {
            wmsMapLayer = dataConnectUtils.createWMSLayer(url, agsAlpha);
        } catch (IOException e) {
            logger.debug(TAG, "addWMSLayer.1.IOException", e.getMessage());
            e.printStackTrace();
        }
        logger.debug(TAG, "addWMSLayer.1.wmsMapLayer", wmsMapLayer);
        if (wmsMapLayer != null) {
            try {
                if (mapService.getVisibleIds() != null) {
                    if (mapService.getVisibleIds().equals("*")) {
                        logger.debug(TAG, "addWMSLayer.1.setVisibilityForWMSLayerAndSubLayers(*)");
                        layerUtils.setVisibilityForWMSLayerAndSubLayers(logger, wmsMapLayer, true);
                    } else {
                        String[] visibleLayerArray = mapService.getVisibleIds().split(",");
                        logger.debug(TAG, "addWMSLayer.1.setVisibilityForWMSLayerAndSubLayers('" + mapService.getVisibleIds() + "')");
                        if (visibleLayerArray.length > 0 && !visibleLayerArray[0].equals("")) {
                            List<String> visibleLayerList = Arrays.asList(visibleLayerArray);
                            layerUtils.setVisibilityForWMSLayerAndSubLayers(logger, wmsMapLayer, visibleLayerList);
                        }
                    }

                } else {
                    logger.debug(TAG, "addWMSLayer.1.setVisibilityForWMSLayerAndSubLayers(true)");
                    layerUtils.setVisibilityForWMSLayerAndSubLayers(logger, wmsMapLayer, true);
                }
                logger.debug(TAG, "addWMSLayer.1.setTransparency", agsAlpha);
                wmsMapLayer.setTransparency(agsAlpha);
            } catch (IOException e) {
                logger.error(TAG, "addWMSLayer1.IOException", e);
                e.printStackTrace();
            }
            logger.debug(TAG, "addWMSLayer.1.wmsMapLayer.OK", wmsMapLayer);
            return wmsMapLayer;
        }
        logger.debug(TAG, "addWMSLayer.2...");
        try {
            logger.debug(TAG, "addWMSLayer.AddWMS", url);

            wmsMapLayer = new WMSMapLayer();
            IWMSConnectionName connName = new WMSConnectionName();
            IPropertySet propSet = new PropertySet();

            String[] paramSet = null;
            boolean foundVersion = false;

            int indexParams = url.lastIndexOf("?");
            String wmsUrl = url;
            if (indexParams != -1) {
                wmsUrl = url.substring(0, indexParams + 1);
                paramSet = url.substring(indexParams + 1, url.length()).split("&");
            }

            if (paramSet != null) {
                logger.debug(TAG, "addWMSLayer.WMS Property Set length", paramSet.length);
                for (String aParamSet : paramSet) {
                    if (aParamSet.toUpperCase().contains("USER")) {
                        int index = aParamSet.toUpperCase().indexOf("USER");
                        String value = aParamSet.substring(index + 5, aParamSet.length());
                        logger.debug(TAG, "addWMSLayer.USER", value);
                        propSet.setProperty("USER", value);
                    } else if (aParamSet.toUpperCase().contains("PASSWORD")) {
                        int index = aParamSet.toUpperCase().indexOf("PASSWORD");
                        String value = aParamSet.substring(index + 9, aParamSet.length());
                        logger.debug(TAG, "addWMSLayer.PASSWORD", value);
                        propSet.setProperty("PASSWORD", value);
                    } else if (aParamSet.toUpperCase().contains("VERSION")) {
                        int index = aParamSet.toUpperCase().indexOf("VERSION");
                        String value = aParamSet.substring(index + 8, aParamSet.length());
                        foundVersion = Boolean.TRUE;
                        logger.debug(TAG, "addWMSLayer.VERSION", value);
                        propSet.setProperty("VERSION", value);
                    } else {
                        wmsUrl += "&" + aParamSet;
                    }
                }
            }
            propSet.setProperty("URL", wmsUrl);

            if (!foundVersion) {
                //needs to have a version information for the connect
                propSet.setProperty("VERSION", "1.3.0");
            }
            connName.setConnectionProperties(propSet);

            //uses the name information to connect to the service
            logger.debug(TAG, "addWMSLayer.Connect to the wms service");
            wmsMapLayer.connect((IName) connName);

            /*
                wmsMapLayer.connect(connName);
                for (int i = 0; i < wmsMapLayer.getCount(); i++) {
                    ILayer layer = wmsMapLayer.getLayer(i);
                    SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
                    IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
                    ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
                    layer.setSpatialReferenceByRef(spatialReference);
                    layer.setVisible(true);
                    if (layer instanceof WMSGroupLayer) {
                        WMSGroupLayer wmsGroupLayer = (WMSGroupLayer) layer;
                        for (int j = 0; j < wmsGroupLayer.getCount(); j++) {
                            ILayer childLayer = wmsGroupLayer.getLayer(j);
                            childLayer.setVisible(true);
                        }
                    }
                }
             */

            //get service description out of the layer
            //the service description contains informations about the wms categories
            //and layers supported by the service
            logger.debug(TAG, "addWMSLayer.Get wms Service Description");
            IWMSServiceDescription serviceDesc = wmsMapLayer.getWMSServiceDescription();

            //for each wms layer either add the stand-alone layer or
            //group layer to the WMSMapLayer which will be added to ArcMap
            logger.debug(TAG, "addWMSLayer.Add each wms layer to a group layer");
            for (int i = 0; i < serviceDesc.getLayerDescriptionCount(); i++) {
                IWMSLayerDescription layerDesc = serviceDesc.getLayerDescription(i);
                ILayer newLayer;
                logger.debug(TAG, "addWMSLayer.Add each wms layer to a group layer.name", layerDesc.getName());
                if (layerDesc.getLayerDescriptionCount() == 0) {
                    IWMSLayer newWMSLayer = wmsMapLayer.createWMSLayer(layerDesc);
                    newLayer = (ILayer) newWMSLayer;
                } else {
                    IWMSGroupLayer grpLayer = wmsMapLayer.createWMSGroupLayers(layerDesc);
                    grpLayer.setExpanded(true);
                    newLayer = (ILayer) grpLayer;
                }
                newLayer.setVisible(true);
                logger.debug(TAG, "addWMSLayer.Add each wms layer to a group layer.newLayer", newLayer.getName());
            }

            // Configure the layer before adding it to the map
            if (serviceDesc.getWMSTitle().length() > 0)
                wmsMapLayer.setName(serviceDesc.getWMSTitle());
            else if (serviceDesc.getWMSName().length() > 0)
                wmsMapLayer.setName(serviceDesc.getWMSName());
            else wmsMapLayer.setName("name: " + mapService.getName() + "");


            // set SpatialEnvironment
            if (mapSpatial != null) {
                logger.debug(TAG, "addWMSLayer.Setting WMS Service Spatial to", mapSpatial.toString());
                wmsMapLayer.setSpatialReferenceByRef(mapSpatial);
            } else {
                SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
                IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
                wmsMapLayer.setSpatialReferenceByRef(geographicCoordinateSystem);
            }

            //Add Layer To map
            wmsMapLayer.setVisible(true);

            //Set visibility
            if (mapService.getVisibleIds() != null) {
                if (mapService.getVisibleIds().equals("*")) {
                    layerUtils.setVisibilityForWMSLayerAndSubLayers(logger, wmsMapLayer, true);
                } else {
                    String[] visibleLayerArray = mapService.getVisibleIds().split(",");
                    if (visibleLayerArray.length > 0 && !visibleLayerArray[0].equals("")) {
                        List<String> visibleLayerList = Arrays.asList(visibleLayerArray);
                        layerUtils.setVisibilityForWMSLayerAndSubLayers(logger, wmsMapLayer, visibleLayerList);
                    }
                }
            } else {
                layerUtils.setVisibilityForWMSLayerAndSubLayers(logger, wmsMapLayer, true);
            }

            ILayer layer = wmsMapLayer;
            if (layer instanceof WMSGroupLayer) {
                WMSGroupLayer wmsGroupLayer = (WMSGroupLayer) layer;
                for (int j = 0; j < wmsGroupLayer.getCount(); j++) {
                    ILayer childLayer = wmsGroupLayer.getLayer(j);
                    childLayer.setVisible(true);
                }
            }
            wmsMapLayer.setTransparency(agsAlpha);
            logger.debug(TAG, "addWMSLayer.OK.returnLayer.name: " + layer.getName() + "  visible" + layer.isVisible() + " valid=" + layer.isValid());
            return layer;
        } catch (IOException e) {
            logger.error(TAG, "addWMSLayer.IOException", e);
        }
        return null;
    }

    //////////////////////////////////////////////////////////////////
    // Add ArcGIS Server Internet Layer Michaël Hansroul 22/02/2011 //
    //////////////////////////////////////////////////////////////////
    private ILayer addAGSInternetLayer(SOELogger logger, short agsAlpha, PchPrintMapService mapService) {
        logger.debug(TAG, "addAGSInternetLayer...");
        String url = mapService.getUrl();
        if (mapService.getProxyURL() != null) {
            url = mapService.getProxyURL() + "?" + url;
        }
        try {
            //create a property set to hold connection properties
            IPropertySet connectionProps = new PropertySet();
            //specify the URL for the server
            int indexServices = url.toUpperCase().indexOf("SERVICES");
            int indexMapServer = url.toUpperCase().indexOf("MAPSERVER");

            String servicesUrl = url.substring(0, indexServices + 8);
            servicesUrl = servicesUrl.replace("rest/", "");

            if (indexServices < 0 || indexMapServer < 0) {
                return null;
            }
            String mapServiceName = url.substring(indexServices + 9, indexMapServer - 1);
            if (mapService.getToken() != null) {
                File jarFolder = new File(PchPrintMapService.class.getProtectionDomain().getCodeSource().getLocation().getFile());
                logger.debug(TAG, "addAGSInternetLayer.securedMapservice.lookingForPropFile.jarFolder1: ", jarFolder);
                if (jarFolder.getAbsolutePath().endsWith(".jar"))
                    jarFolder = jarFolder.getParentFile();
                logger.debug(TAG, "addAGSInternetLayer.securedMapservice.lookingForPropFile.jarFolder2: ", jarFolder);
                String propUser = propertiesFileHelper.getValue(logger, "user");
                String propPassword = propertiesFileHelper.getValue(logger, "password");
                if (propUser != null && propPassword != null) {
                    logger.debug(TAG, "addAGSInternetLayer.securedMapservice.propUser", propUser);
                    logger.debug(TAG, "addAGSInternetLayer.securedMapservice.propPassword", propPassword);
                    connectionProps.setProperty("USER", propUser);
                    connectionProps.setProperty("PASSWORD", propPassword);
                }
            }
            if (mapService.getProxyURL() != null) {
                servicesUrl = mapService.getProxyURL() + "?" + servicesUrl;
            }
            logger.debug(TAG, "addAGSInternetLayer.servicesUrl", servicesUrl);

            connectionProps.setProperty("URL", servicesUrl);
            connectionProps.setProperty("CONNECTIONTYPE", "esriConnectionTypeInternet");

            IAGSServerConnectionFactory connectionFactory = new AGSServerConnectionFactory();
            IAGSServerConnection gisServer = connectionFactory.open(connectionProps, 0);

            IAGSEnumServerObjectName soNames = gisServer.getServerObjectNames();
            IAGSServerObjectName soName;


            soName = soNames.next();
            do {
                //logger.debug(soName.getName().toUpperCase() + " # " + mapServiceName.toUpperCase());
                if ((soName.getType().equals("MapServer")) && (soName.getName().toUpperCase().equals(mapServiceName.toUpperCase()))) {
                    break;  //found it
                }
                //keep searching the services ...
                soName = soNames.next();
            } while (soName != null);
            if (soName != null) {
                IName name = (IName) soName;
                IMapServer mapserver = (IMapServer) name.open();

                String mapname = mapserver.getDefaultMapName();

                MapServerLayer mapServerLayer = new MapServerLayer();
                mapServerLayer.serverConnect(soName, mapname);

                //Set Visible layer in the mapservice layer
                String mapServiceVisibleIds = mapService.getVisibleIds();
                configureVisibilityForMapService(logger, mapServerLayer, mapServiceVisibleIds);

                mapServerLayer.setTransparency(agsAlpha);
                if (mapServerLayer.isValid()) {
                    return mapServerLayer;
                }
            }
        } catch (IOException e) {
            logger.error(TAG, "addAGSInternetLayer.IOException", e);
        }
        return null;
    }

    private void configureVisibilityForMapService(SOELogger logger, MapServerLayer layer, String mapServiceVisibleIds) throws IOException {
        logger.debug(TAG, "configureVisibilityForMapService...");
        if (mapServiceVisibleIds != null && mapServiceVisibleIds.length() > 0) {
            logger.debug(TAG, "configureVisibilityForMapService.mapServiceVisibleIds.length", mapServiceVisibleIds.length());
            HashMap<Integer, ILayer> layerIdsMap = layerUtils.getAllLayersById(logger, layer);
            logger.debug(TAG, "configureVisibilityForMapService.layerIdsMap.length", layerIdsMap.size());
            if (mapServiceVisibleIds.equals("*")) {
                logger.debug(TAG, "configureVisibilityForMapService.*");
                for (ILayer iLayer : layerIdsMap.values()) {
                    iLayer.setVisible(true);
                }
            } else {
                for (ILayer iLayer : layerIdsMap.values()) {
                    iLayer.setVisible(false);
                }
                String[] visibleLayerList = mapServiceVisibleIds.split(",");
                logger.debug(TAG, "configureVisibilityForMapService.visibleLayerList.length", visibleLayerList.length);
                if (visibleLayerList.length > 0) {
                    List<Integer> listVisibleLayer = new ArrayList<Integer>();
                    for (String aVisibleLayerList : visibleLayerList) {
                        if (aVisibleLayerList.trim().length() > 0)
                            listVisibleLayer.add(Integer.decode(aVisibleLayerList));
                    }
                    HashMap<Integer, Integer> parentLayerIdsMap = layerUtils.getParentLayerIds(logger, layer);

                    for (Integer visibleLayerId : listVisibleLayer) {
                        layerIdsMap.get(visibleLayerId).setVisible(true);
                        logger.debug(TAG, "configureVisibilityForMapService.makeLayerVisible.visibleLayerId", visibleLayerId);
                        Integer parentLayerId;
                        while ((parentLayerId = parentLayerIdsMap.get(visibleLayerId)) != null) {
                            layerIdsMap.get(parentLayerId).setVisible(true);
                            logger.debug(TAG, "configureVisibilityForMapService.makeParentVisible.parentLayerId", parentLayerId);
                            visibleLayerId = parentLayerId;
                        }
                    }
                }
            }
        } else {
            logger.warning(TAG, "configureVisibilityForMapService.mapServiceVisibleIds.INVALID", mapServiceVisibleIds);
        }
        logger.debug(TAG, "configureVisibilityForMapService.END");
    }

    public boolean layerIsValid(SOELogger logger, ILayer layer) throws IOException {
        if (layer == null) {
            logger.debug(TAG, "layerIsValid.layer: >null<");
            return false;
        }
        logger.debug(TAG, "layerIsValid.layer[" + layer.getName() + "].minimumScale: " + layer.getMinimumScale());
        logger.debug(TAG, "layerIsValid.layer[" + layer.getName() + "].maximumScale: " + layer.getMaximumScale());
        if (!layer.isValid()) {
            logger.debug(TAG, "layerIsInvalid", layer);
            return false;
        }
        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                if (!layerIsValid(logger, compositeLayer.getLayer(i))) return false;
            }
        }
        return true;
    }

    private ILayer downloadAndLoadLyrFile(SOELogger logger, String layerFileName, String refererWebsite, String authorization401) throws IOException {
        logger.debug(TAG, "downloadAndLoadLyrFile.layerFileName", layerFileName);
        logger.debug(TAG, "downloadAndLoadLyrFile.refererWebsite", refererWebsite);
        File agsTmpFile = File.createTempFile("PCHPrintSOELayer", ".lyr");
        logger.debug(TAG, "downloadAndLoadLyrFile.agsTmpFile", agsTmpFile);
        RESTUtils.downloadFile(logger, layerFileName, agsTmpFile, refererWebsite, authorization401);
        String agsTmpFileName = agsTmpFile.getAbsolutePath();
        logger.debug(TAG, "downloadAndLoadLyrFile.agsTmpFileName", agsTmpFileName);
        ILayer layer = null;
        LayerFile layerFile = new LayerFile();
        if (layerFile.isLayerFile(agsTmpFileName)) {
            layerFile.open(agsTmpFileName);
            layer = layerFile.getLayer();
            layerFile.close();
        } else {
            logger.error(TAG, "PchPrintMapService.downloadAndLoadLyrFile.is NOT an layerFile", agsTmpFileName);
        }
        logger.debug(TAG, "downloadAndLoadLyrFile.cleanUp.deleting", agsTmpFileName);
        boolean deleted = agsTmpFile.delete();
        logger.debug(TAG, "downloadAndLoadLyrFile.cleanUp.deleted", deleted);
        return layer;
    }

    private String getLyrFileReplacement(SOELogger logger, PchPrintMapService mapService) {
        String key = mapService.getType() + "_" + mapService.getUrl() + (mapService.getVisibleIds() != null ? "_" + mapService.getVisibleIds() : "");
        logger.debug(TAG, "getLyrFileReplacement.key: ", key);
        String value = propertiesFileHelper.getValue(logger, key);
        logger.debug(TAG, "getLyrFileReplacement.value: ", value);
        if (value != null) {
            return value;
        }
        return null;
    }
}
