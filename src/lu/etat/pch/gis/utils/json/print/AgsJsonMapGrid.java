/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.print;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ILineSymbol;
import com.esri.arcgis.geometry.ISpatialReference;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.ServerUtilities;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.format.AgsJsonGridLabelFormat;
import lu.etat.pch.gis.utils.json.graphics.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 3/31/12
 * Time: 6:41 AM
 */
public class AgsJsonMapGrid {
    private static final String TAG = "AgsJsonMapGrid";
    private SOELogger logger;
    private String gridType = "measured";   // graticule
    private String label;
    private Double xIntervalSize;
    private Double yIntervalSize;
    private AgsJsonGridLabelFormat labelFormat;
    private boolean labelAxisTop = true;
    private boolean labelAxisLeft = true;
    private boolean labelAxisRight = true;
    private boolean labelAxisBottom = true;
    private int spatialReferenceWKID = 0;
    private Double xOrigin;
    private Double yOrigin;
    private AgsJsonLineSymbol lineSymbol;
    private AgsJsonMarkerSymbol tickMarkSymbol;
    private AgsJsonSimpleLineSymbol border;

    public AgsJsonMapGrid(SOELogger logger) {
        this.logger = logger;
    }

    public AgsJsonMapGrid(SOELogger logger, JSONObject jsonObject) {
        this.logger = logger;
        if (!jsonObject.isNull("gridType")) gridType = jsonObject.getString("gridType");
        if (!jsonObject.isNull("label")) label = jsonObject.getString("label");
        if (!jsonObject.isNull("xIntervalSize")) xIntervalSize = jsonObject.getDouble("xIntervalSize");
        if (!jsonObject.isNull("yIntervalSize")) yIntervalSize = jsonObject.getDouble("yIntervalSize");
        if (!jsonObject.isNull("labelFormat"))
            labelFormat = new AgsJsonGridLabelFormat(logger, jsonObject.getJSONObject("labelFormat"));
        if (!jsonObject.isNull("labelAxisTop")) labelAxisTop = jsonObject.getBoolean("labelAxisTop");
        if (!jsonObject.isNull("labelAxisLeft")) labelAxisLeft = jsonObject.getBoolean("labelAxisLeft");
        if (!jsonObject.isNull("labelAxisRight")) labelAxisRight = jsonObject.getBoolean("labelAxisRight");
        if (!jsonObject.isNull("labelAxisBottom")) labelAxisBottom = jsonObject.getBoolean("labelAxisBottom");
        if (!jsonObject.isNull("spatialReferenceWKID"))
            spatialReferenceWKID = jsonObject.getInt("spatialReferenceWKID");
        if (!jsonObject.isNull("xOrigin")) xOrigin = jsonObject.getDouble("xOrigin");
        if (!jsonObject.isNull("yOrigin")) yOrigin = jsonObject.getDouble("yOrigin");
        if (!jsonObject.isNull("lineSymbol"))
            lineSymbol = LineSymbols.getSymbol(logger, jsonObject.getJSONObject("lineSymbol"));
        if (!jsonObject.isNull("tickMarkSymbol"))
            tickMarkSymbol = MarkerSymbols.getSymbol(logger, jsonObject.getJSONObject("tickMarkSymbol"));
        if (!jsonObject.isNull("border"))
            border = new AgsJsonSimpleLineSymbol(logger, jsonObject.getJSONObject("border"));
    }

    public IMapGrid toArcObject() {
        logger.debug(TAG, "toArcObject()...");
        IMapGrid mapGrid = null;
        try {
            if (gridType.equalsIgnoreCase("measured")) {
                logger.debug(TAG, "toArcObject().measured");
                MeasuredGrid mGrid = new MeasuredGrid();
                if (spatialReferenceWKID > 0) {
                    try {
                        ISpatialReference spatRef = ServerUtilities.getSRFromString("" + spatialReferenceWKID);
                        mGrid.setSpatialReferenceByRef(spatRef);
                        logger.debug(TAG, "toArcObject().spatRef", spatRef.getName());
                    } catch (Exception e) {
                        logger.error(TAG, "toArcObject.Error in spatialReferenceWKID", spatialReferenceWKID);
                        logger.error(TAG, "toArcObject", e);
                    }
                }
                mapGrid = mGrid;
            } else if (gridType.equalsIgnoreCase("graticule")) {
                logger.debug(TAG, "toArcObject().graticule");
                Graticule graticule = new Graticule();
                graticule.setName("GraticuleGrid");
                mapGrid = graticule;
            } else {
                logger.error(TAG, "_ONLY_measured_and graticule_CURRENTLY_suported.gridType", gridType);
            }
            if (labelFormat != null) mapGrid.setLabelFormat(labelFormat.toArcObjects(mapGrid));
            if (lineSymbol != null) {
                mapGrid.setLineSymbol(lineSymbol.toLineArcObject().get(0));
            } else {
                mapGrid.setLineSymbol(null);
            }
            if (tickMarkSymbol != null) mapGrid.setTickMarkSymbol(tickMarkSymbol.toMarkerArcObject());
            if (label != null) mapGrid.setName(label);
            mapGrid.setLabelVisibility(labelAxisLeft, labelAxisTop, labelAxisRight, labelAxisBottom);
            if (border != null) {
                SimpleMapGridBorder mapGridBorder = new SimpleMapGridBorder();
                mapGridBorder.setLineSymbol((ILineSymbol) border.toArcObject().get(0));
                mapGrid.setBorder(mapGridBorder);
            }
            if (mapGrid instanceof IMeasuredGrid) {
                IMeasuredGrid measuredGrid = (IMeasuredGrid) mapGrid;
                if (xIntervalSize != null) {
                    measuredGrid.setXIntervalSize(xIntervalSize);
                    if (yIntervalSize == null)
                        yIntervalSize = xIntervalSize;
                    measuredGrid.setYIntervalSize(yIntervalSize);
                }
                if (xOrigin != null && yOrigin != null) {
                    measuredGrid.setXOrigin(xOrigin);
                    measuredGrid.setYOrigin(yOrigin);
                    measuredGrid.setFixedOrigin(true);
                } else measuredGrid.setFixedOrigin(false);
            }
        } catch (AutomationException e) {
            e.printStackTrace();
            logger.error(TAG, "toArcObject().AutomationException", e);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            logger.error(TAG, "toArcObject().UnknownHostException", e);
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(TAG, "toArcObject().IOException", e);

        }
        logger.debug(TAG, "toArcObject().DONE");
        return mapGrid;
    }

    public String getGridType() {
        return gridType;
    }

    public void setGridType(String gridType) {
        this.gridType = gridType;
    }

    public Double getXIntervalSize() {
        return xIntervalSize;
    }

    public void setXIntervalSize(Double xIntervalSize) {
        this.xIntervalSize = xIntervalSize;
    }

    public Double getYIntervalSize() {
        return yIntervalSize;
    }

    public void setYIntervalSize(Double yIntervalSize) {
        this.yIntervalSize = yIntervalSize;
    }

    public AgsJsonGridLabelFormat getLabelFormat() {
        return labelFormat;
    }

    public void setLabelFormat(AgsJsonGridLabelFormat labelFormat) {
        this.labelFormat = labelFormat;
    }

    public int getSpatialReferenceWKID() {
        return spatialReferenceWKID;
    }

    public void setSpatialReferenceWKID(int spatialReferenceWKID) {
        this.spatialReferenceWKID = spatialReferenceWKID;
    }

    public boolean isLabelAxisBottom() {
        return labelAxisBottom;
    }

    public void setLabelAxisBottom(boolean labelAxisBottom) {
        this.labelAxisBottom = labelAxisBottom;
    }

    public boolean isLabelAxisRight() {
        return labelAxisRight;
    }

    public void setLabelAxisRight(boolean labelAxisRight) {
        this.labelAxisRight = labelAxisRight;
    }

    public boolean isLabelAxisLeft() {
        return labelAxisLeft;
    }

    public void setLabelAxisLeft(boolean labelAxisLeft) {
        this.labelAxisLeft = labelAxisLeft;
    }

    public boolean isLabelAxisTop() {
        return labelAxisTop;
    }

    public void setLabelAxisTop(boolean labelAxisTop) {
        this.labelAxisTop = labelAxisTop;
    }

    public static List<AgsJsonMapGrid> parseJsonArray(SOELogger logger, JSONArray mapGridsArray) {
        List<AgsJsonMapGrid> mapGridList = new ArrayList<AgsJsonMapGrid>();
        if (mapGridsArray == null) return mapGridList;
        for (int i = 0; i < mapGridsArray.length(); i++) {
            mapGridList.add(new AgsJsonMapGrid(logger, mapGridsArray.getJSONObject(i)));
        }
        return mapGridList;
    }
}