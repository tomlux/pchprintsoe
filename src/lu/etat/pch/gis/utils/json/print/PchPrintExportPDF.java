/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.print;

import com.esri.arcgis.display.esriPictureSymbolOptions;
import com.esri.arcgis.output.ExportPDF;
import com.esri.arcgis.output.esriExportColorspace;
import com.esri.arcgis.output.esriExportImageCompression;
import com.esri.arcgis.output.esriExportPDFLayerOptions;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 3/11/11
 * Time: 5:54 AM
 */
public class PchPrintExportPDF extends AbstractPchPrintExportSettings {
    private int colorspace = esriExportColorspace.esriExportColorspaceRGB; // esriExportColorspace
    private boolean compressed = true;
    private int imageCompression = esriExportImageCompression.esriExportImageCompressionNone; //esriExportImageCompression
    private int exportPictureSymbolOptions = esriPictureSymbolOptions.esriPSOVectorize; //esriPictureSymbolOptions
    private boolean polyginizeMarkers = false;
    private boolean embedFonts = true;
    private int exportPDFLayersAndFeatureAttributes = esriExportPDFLayerOptions.esriExportPDFLayerOptionsNone; //esriExportPDFLayerOptions
    private boolean exportMeasureInfo = false;

    public PchPrintExportPDF() {
    }

    public PchPrintExportPDF(JSONObject jsonObj) {
        if (!jsonObj.isNull("colorspace")) colorspace = jsonObj.getInt("colorspace");
        if (!jsonObj.isNull("compressed")) compressed = jsonObj.getBoolean("compressed");
        if (!jsonObj.isNull("imageCompression")) imageCompression = jsonObj.getInt("imageCompression");
        if (!jsonObj.isNull("exportPictureSymbolOptions"))
            exportPictureSymbolOptions = jsonObj.getInt("exportPictureSymbolOptions");
        if (!jsonObj.isNull("polyginizeMarkers")) polyginizeMarkers = jsonObj.getBoolean("polyginizeMarkers");
        if (!jsonObj.isNull("embedFonts")) embedFonts = jsonObj.getBoolean("embedFonts");
        if (!jsonObj.isNull("exportPDFLayersAndFeatureAttributes"))
            exportPDFLayersAndFeatureAttributes = jsonObj.getInt("exportPDFLayersAndFeatureAttributes");
        if (!jsonObj.isNull("exportMeasureInfo")) exportMeasureInfo = jsonObj.getBoolean("exportMeasureInfo");

    }

    public JSONObject toJSON() {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("colorspace", colorspace);
        jsonObj.put("compressed", compressed);
        jsonObj.put("imageCompression", imageCompression);
        jsonObj.put("exportPictureSymbolOptions", exportPictureSymbolOptions);
        jsonObj.put("polyginizeMarkers", polyginizeMarkers);
        jsonObj.put("embedFonts", embedFonts);
        jsonObj.put("exportPDFLayersAndFeatureAttributes", exportPDFLayersAndFeatureAttributes);
        jsonObj.put("exportMeasureInfo", exportMeasureInfo);
        return jsonObj;
    }

    public ExportPDF toArcObject() throws IOException {
        ExportPDF exportPDF = new ExportPDF();
        exportPDF.setColorspace(colorspace);
        exportPDF.setCompressed(compressed);
        exportPDF.setImageCompression(imageCompression);
        exportPDF.setExportPictureSymbolOptions(exportPictureSymbolOptions);
        exportPDF.setPolygonizeMarkers(polyginizeMarkers);
        exportPDF.setEmbedFonts(embedFonts);
        exportPDF.setExportPDFLayersAndFeatureAttributes(exportPDFLayersAndFeatureAttributes);
        exportPDF.setExportMeasureInfo(exportMeasureInfo);
        return exportPDF;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PchPrintExportPDF");
        sb.append("{colorspace=").append(colorspace);
        sb.append(", compressed=").append(compressed);
        sb.append(", imageCompression=").append(imageCompression);
        sb.append(", exportPictureSymbolOptions=").append(exportPictureSymbolOptions);
        sb.append(", polyginizeMarkers=").append(polyginizeMarkers);
        sb.append(", embedFonts=").append(embedFonts);
        sb.append(", exportPDFLayersAndFeatureAttributes=").append(exportPDFLayersAndFeatureAttributes);
        sb.append(", exportMeasureInfo=").append(exportMeasureInfo);
        sb.append('}');
        return sb.toString();
    }
}
