/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.INumberFormat;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.NumericFormat;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.UnitConverter;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;
import lu.etat.pch.gis.utils.json.format.AgsJsonNumericFormat;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 8, 2010
 * Time: 5:38:35 AM
 */
public class AgsJsonScaleBar extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonScaleBar";
    private static HashMap<String, String> scaleBarTypes = new HashMap<String, String>();

    static {
        scaleBarTypes.put("AlternatingScaleBar", AlternatingScaleBar.getClsid());
        scaleBarTypes.put("DoubleAlternatingScaleBar", DoubleAlternatingScaleBar.getClsid());
        scaleBarTypes.put("HollowScaleBar", HollowScaleBar.getClsid());
        scaleBarTypes.put("ScaleLine", ScaleLine.getClsid());
        scaleBarTypes.put("SingleDivisionScaleBar", SingleDivisionScaleBar.getClsid());
        scaleBarTypes.put("SteppedScaleLine", SteppedScaleLine.getClsid());
    }

    public static final String TYPE = "pchScaleBar";
    private String scaleBarType = "AlternatingScaleBar";
    private Double barHeight = 5d;
    private String units;
    private AgsJsonRGBColor barColor;
    private Double division;
    private Short divisions;
    private Short divisionsBeforeZero;
    private Integer labelFrequency;
    private Integer labelPosition;
    private Double labelGap;
    private AgsJsonTextSymbol labelSymbol;
    private AgsJsonNumericFormat numberFormat;
    private Integer resizeHint;
    private Short subdivisions;
    private String unitLabel;
    private Double unitLabelGap;
    private Integer unitLabelPosition;
    private AgsJsonTextSymbol unitLabelSymbol;
    private AgsJsonRGBColor backgroundColor;


    public AgsJsonScaleBar(SOELogger logger) {
        super(TYPE, logger);
    }

    public AgsJsonScaleBar(SOELogger logger, double barHeight) {
        super(TYPE, logger);
        this.barHeight = barHeight;
    }

    public AgsJsonScaleBar(SOELogger logger, JSONObject jsonObject) {
        super(TYPE, logger);
        try {
            if (!jsonObject.isNull("scaleBarType")) {
                String tmpScaleBarType = jsonObject.getString("scaleBarType");
                for (String barType : scaleBarTypes.keySet()) {
                    if (barType.equalsIgnoreCase(tmpScaleBarType)) {
                        scaleBarType = barType;
                        break;
                    }
                }
            }
            if (!jsonObject.isNull("barHeight")) this.barHeight = jsonObject.getDouble("barHeight");
            if (!jsonObject.isNull("unit")) this.units = jsonObject.getString("unit");
            if (!jsonObject.isNull("units")) this.units = jsonObject.getString("units");
            if (!jsonObject.isNull("barColor")) this.barColor = new AgsJsonRGBColor(logger, jsonObject.get("barColor"));
            if (!jsonObject.isNull("division")) this.division = jsonObject.getDouble("division");
            if (!jsonObject.isNull("divisions")) this.divisions = new Short("" + jsonObject.getInt("divisions"));
            if (!jsonObject.isNull("divisionsBeforeZero"))
                this.divisionsBeforeZero = new Short("" + jsonObject.getInt("divisionsBeforeZero"));
            if (!jsonObject.isNull("labelFrequency")) this.labelFrequency = jsonObject.getInt("labelFrequency");
            if (!jsonObject.isNull("labelPosition")) this.labelPosition = jsonObject.getInt("labelPosition");
            if (!jsonObject.isNull("labelGap")) this.labelGap = jsonObject.getDouble("labelGap");
            if (!jsonObject.isNull("labelSymbol"))
                this.labelSymbol = new AgsJsonTextSymbol(logger, jsonObject.getJSONObject("labelSymbol"));
            if (!jsonObject.isNull("numberFormat"))
                this.numberFormat = new AgsJsonNumericFormat(logger, jsonObject.getJSONObject("numberFormat"));
            if (!jsonObject.isNull("resizeHint")) this.resizeHint = jsonObject.getInt("resizeHint");
            if (!jsonObject.isNull("subdivisions"))
                this.subdivisions = new Short("" + jsonObject.getInt("subdivisions"));
            if (!jsonObject.isNull("unitLabel")) this.unitLabel = jsonObject.getString("unitLabel");
            if (!jsonObject.isNull("unitLabelGap")) this.unitLabelGap = jsonObject.getDouble("unitLabelGap");
            if (!jsonObject.isNull("unitLabelPosition"))
                this.unitLabelPosition = jsonObject.getInt("unitLabelPosition");
            if (!jsonObject.isNull("unitLabelSymbol"))
                this.unitLabelSymbol = new AgsJsonTextSymbol(logger, jsonObject.getJSONObject("unitLabelSymbol"));
            if (!jsonObject.isNull("backgroundColor")) {
                backgroundColor = new AgsJsonRGBColor(logger, jsonObject.getJSONObject("backgroundColor"));
            }
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonScaleBar(JSONObject)", e);
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        if (barHeight != null) symbObject.put("barHeight", barHeight);
        if (units != null) symbObject.put("units", units);
        if (backgroundColor != null) symbObject.put("backgroundColor", backgroundColor.toJSON());
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame, IGeometry geom) throws IOException {
        IUID scaleBarUID = new UID();
        scaleBarUID.setValue(scaleBarTypes.get(scaleBarType));

        MapSurroundFrame scaleBarFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(scaleBarUID, null);
        IScaleBar scaleBar = (IScaleBar) scaleBarFrame.getMapSurround();
        scaleBar.useMapSettings();
        if (barHeight != null) {
            scaleBar.setBarHeight(barHeight);
            ITextSymbol labelTextSymbol = scaleBar.getLabelSymbol();
            labelTextSymbol.setSize(barHeight * 2);
            scaleBar.setUnitLabelSymbol(labelTextSymbol);
            scaleBar.setLabelSymbol(labelTextSymbol);
        }
        if (barColor != null) scaleBar.setBarColor(barColor.toArcObject());
        if (division != null) scaleBar.setDivision(division);
        if (divisions != null) scaleBar.setDivisions(divisions);
        if (divisionsBeforeZero != null) scaleBar.setDivisionsBeforeZero(divisionsBeforeZero);
        if (labelFrequency != null) scaleBar.setLabelFrequency(labelFrequency);
        if (labelPosition != null) scaleBar.setLabelPosition(labelPosition);
        if (labelGap != null) scaleBar.setLabelGap(labelGap);
        if (labelSymbol != null) scaleBar.setLabelSymbol((ITextSymbol) labelSymbol.toArcObject().get(0));
        if (numberFormat != null) {
            INumberFormat theNumberFormat = scaleBar.getNumberFormat();
            if (theNumberFormat instanceof NumericFormat) {
                scaleBar.setNumberFormat(numberFormat.toArcObjects((NumericFormat) scaleBar.getNumberFormat()));
            } else logger.error(TAG, "toArcobjects.theNumberFormat NOT numericFormat", theNumberFormat);
        }
        if (resizeHint != null) scaleBar.setResizeHint(resizeHint);
        if (subdivisions != null) scaleBar.setSubdivisions(subdivisions);

        if (backgroundColor != null) {
            SymbolBackground symbolBackground = new SymbolBackground();
            symbolBackground.setColor(backgroundColor.toArcObject());
            scaleBarFrame.setBackground(symbolBackground);
        }
        if (this.units != null) {
            int units = UnitConverter.getEsriUnitFromString(logger, this.units);
            scaleBar.setUnits(units);
        }
        if (unitLabel != null) scaleBar.setUnitLabel(unitLabel);
        if (unitLabelGap != null) scaleBar.setUnitLabelGap(unitLabelGap);
        if (unitLabelPosition != null) scaleBar.setUnitLabelPosition(unitLabelPosition);
        if (unitLabelSymbol != null) scaleBar.setUnitLabelSymbol((ITextSymbol) unitLabelSymbol.toArcObject().get(0));
        return scaleBarFrame;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public String getScaleBarType() {
        return scaleBarType;
    }

    public void setScaleBarType(String scaleBarType) {
        this.scaleBarType = scaleBarType;
    }

    public double getBarHeight() {
        return barHeight;
    }

    public void setBarHeight(double barHeight) {
        this.barHeight = barHeight;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public AgsJsonRGBColor getBarColor() {
        return barColor;
    }

    public void setBarColor(AgsJsonRGBColor barColor) {
        this.barColor = barColor;
    }

    public Double getDivision() {
        return division;
    }

    public void setDivision(Double division) {
        this.division = division;
    }

    public Short getDivisions() {
        return divisions;
    }

    public void setDivisions(Short divisions) {
        this.divisions = divisions;
    }

    public Short getDivisionsBeforeZero() {
        return divisionsBeforeZero;
    }

    public void setDivisionsBeforeZero(Short divisionsBeforeZero) {
        this.divisionsBeforeZero = divisionsBeforeZero;
    }

    public Integer getLabelFrequency() {
        return labelFrequency;
    }

    public void setLabelFrequency(Integer labelFrequency) {
        this.labelFrequency = labelFrequency;
    }

    public Integer getLabelPosition() {
        return labelPosition;
    }

    public void setLabelPosition(Integer labelPosition) {
        this.labelPosition = labelPosition;
    }

    public Double getLabelGap() {
        return labelGap;
    }

    public void setLabelGap(Double labelGap) {
        this.labelGap = labelGap;
    }

    public AgsJsonTextSymbol getLabelSymbol() {
        return labelSymbol;
    }

    public void setLabelSymbol(AgsJsonTextSymbol labelSymbol) {
        this.labelSymbol = labelSymbol;
    }

    public AgsJsonNumericFormat getNumberFormat() {
        return numberFormat;
    }

    public void setNumberFormat(AgsJsonNumericFormat numberFormat) {
        this.numberFormat = numberFormat;
    }

    public Integer getResizeHint() {
        return resizeHint;
    }

    public void setResizeHint(Integer resizeHint) {
        this.resizeHint = resizeHint;
    }

    public Short getSubdivisions() {
        return subdivisions;
    }

    public void setSubdivisions(Short subdivisions) {
        this.subdivisions = subdivisions;
    }

    public String getUnitLabel() {
        return unitLabel;
    }

    public void setUnitLabel(String unitLabel) {
        this.unitLabel = unitLabel;
    }

    public Double getUnitLabelGap() {
        return unitLabelGap;
    }

    public void setUnitLabelGap(Double unitLabelGap) {
        this.unitLabelGap = unitLabelGap;
    }

    public Integer getUnitLabelPosition() {
        return unitLabelPosition;
    }

    public void setUnitLabelPosition(Integer unitLabelPosition) {
        this.unitLabelPosition = unitLabelPosition;
    }

    public AgsJsonTextSymbol getUnitLabelSymbol() {
        return unitLabelSymbol;
    }

    public void setUnitLabelSymbol(AgsJsonTextSymbol unitLabelSymbol) {
        this.unitLabelSymbol = unitLabelSymbol;
    }

    public AgsJsonRGBColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(AgsJsonRGBColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

}