/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ICharacterMarkerSymbol;
import com.esri.arcgis.display.IMarkerSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.support.ms.stdole.StdFont;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/22/11
 * Time: 6:25 AM
 */
public class AgsJsonNorthArrow extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonNorthArrow";
    public static final String TYPE = "pchNorthArrow";

    private String fontFamily = "ESRI North";
    private int charIndex = 200;
    private double size = -1;
    private AgsJsonRGBColor backgroundColor;

    public AgsJsonNorthArrow(SOELogger logger) {
        super(TYPE, logger);
    }

    public AgsJsonNorthArrow(SOELogger logger, JSONObject jsonObject) {
        super(TYPE, logger);
        try {
            if (!jsonObject.isNull("fontFamily")) this.fontFamily = jsonObject.getString("fontFamily");
            if (!jsonObject.isNull("charIndex")) this.charIndex = jsonObject.getInt("charIndex");
            if (!jsonObject.isNull("size")) this.size = jsonObject.getDouble("size");
            if (!jsonObject.isNull("backgroundColor")) {
                backgroundColor = new AgsJsonRGBColor(logger, jsonObject.getJSONObject("backgroundColor"));
            }
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonNorthArrow(JSONObject)", e);
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("fontFamily", fontFamily);
        symbObject.put("charIndex", charIndex);
        symbObject.put("size", size);
        if (backgroundColor != null) symbObject.put("backgroundColor", backgroundColor.toJSON());
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame,IGeometry geom) throws IOException {
        IUID northArrowUID = new UID();
        northArrowUID.setValue(MarkerNorthArrow.getClsid());

        MapSurroundFrame northArrowFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(northArrowUID, null);
        MarkerNorthArrow northArrow = (MarkerNorthArrow) northArrowFrame.getMapSurround();

        IMarkerSymbol markerSymbol = northArrow.getMarkerSymbol();
        logger.debug(TAG, "toArcObject().markerSymbol: " + markerSymbol);
        ICharacterMarkerSymbol characterMarkerSymbol = (ICharacterMarkerSymbol) markerSymbol;
        StdFont font = new StdFont();
        font.setName(fontFamily);
        characterMarkerSymbol.setFont(font);
        characterMarkerSymbol.setCharacterIndex(charIndex);
        northArrow.setMarkerSymbol(characterMarkerSymbol);
        if (size > -1) {
            northArrow.setSize(size);
        }

        logger.debug(TAG, "toArcObject.characterMarkerSymbol.fontFamily", characterMarkerSymbol.getFont().getName());
        logger.debug(TAG, "toArcObject.characterMarkerSymbol.characterIndex", characterMarkerSymbol.getCharacterIndex());
        logger.debug(TAG, "toArcObject.characterMarkerSymbol.size", northArrow.getSize());

        if (backgroundColor != null) {
            logger.debug(TAG, "toArcObject.characterMarkerSymbol.backgroundColor", backgroundColor);
            SymbolBackground symbolBackground = new SymbolBackground();
            symbolBackground.setColor(backgroundColor.toArcObject());
            northArrowFrame.setBackground(symbolBackground);
        }
        return northArrowFrame;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public int getCharIndex() {
        return charIndex;
    }

    public void setCharIndex(int charIndex) {
        this.charIndex = charIndex;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public AgsJsonRGBColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(AgsJsonRGBColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
