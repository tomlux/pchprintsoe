/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.IElement;
import com.esri.arcgis.carto.IMapFrame;
import com.esri.arcgis.carto.PageLayout;
import com.esri.arcgis.carto.PictureElement;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.UnitConverter;
import com.esri.arcgis.system.esriUnits;
import lu.etat.pch.gis.utils.SOELogger;
import uk.co.jaimon.test.SimpleImageInfo;

import java.io.*;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 4/1/11
 * Time: 4:22 PM
 */
public class AgsJsonPictureElement extends AbstractAgsJsonLayoutElement {
    private static final String TAG = "AgsJsonPictureElement";
    public static final String TYPE = "pchPictureElement";
    private String pictureUrl;

    public AgsJsonPictureElement(SOELogger logger, String pictureUrl) {
        super(TYPE, logger);
        this.pictureUrl = pictureUrl;
    }

    public AgsJsonPictureElement(SOELogger logger, JSONObject jsonObj) {
        super(TYPE, logger);
        try {
            if (!jsonObj.isNull("pictureUrl")) this.pictureUrl = jsonObj.getString("pictureUrl");
        } catch (JSONException e) {
            logger.error(TAG, "AgsJsonPictureElement(JSONObject)", e);
        }
    }


    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("pictureUrl", pictureUrl);
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IMapFrame mapFrame,IGeometry geom) throws IOException {
        File pictureFile = new File(pictureUrl);
        logger.debug(TAG, "toArcObject.pictureFile.canRead('" + pictureUrl + "') = " + pictureFile.canRead());
        String pictureTmpFile = null;
        if (pictureFile.canRead()) {
            pictureTmpFile = pictureFile.getAbsolutePath();
        } else {
            try {
                URL url = new URL(pictureUrl);
                logger.debug(TAG, "toArcObject.url = " + url);
                //Image img = ImageIO.read(pictureFile);
                File tmpFile = File.createTempFile("pictureElem", pictureUrl.substring(pictureUrl.lastIndexOf(".")));
                logger.debug(TAG, "toArcObject.tmpFile = " + tmpFile);
                BufferedInputStream in = new BufferedInputStream(url.openStream());
                FileOutputStream fos = new java.io.FileOutputStream(tmpFile);
                BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
                byte[] data = new byte[1024];
                int x = 0;
                while ((x = in.read(data, 0, 1024)) >= 0) {
                    bout.write(data, 0, x);
                }
                bout.close();
                in.close();
                pictureTmpFile = tmpFile.getAbsolutePath();
            } catch (UnknownHostException e) {
                logger.error(TAG, "toArcObject.UnknownHostException: " + e.getMessage());
                return null;
            } catch (IOException e) {
                logger.error(TAG, "toArcObject.IOException: " + e.getMessage());
                return null;
            }
        }
        if (pictureTmpFile != null) {
            logger.debug(TAG, "toArcObject.pictureTmpFile.pictureTmpFile: " + pictureTmpFile);
            PictureElement pictureElement = new PictureElement();
            pictureElement.importPictureFromFile(pictureTmpFile);
            IEnvelope envelope = null;
            if (geom instanceof IPoint) {
                IPoint pt1 = (IPoint) geom;
                SimpleImageInfo simpleImageInfo = new SimpleImageInfo(new File(pictureTmpFile));
                int heightPx = simpleImageInfo.getHeight();
                int widthPx = simpleImageInfo.getWidth();
                double heightInch = heightPx / pageLayout.getScreenDisplay().getDisplayTransformation().getResolution();
                double widthInch = widthPx / pageLayout.getScreenDisplay().getDisplayTransformation().getResolution();
                UnitConverter unitConverter = new UnitConverter();
                double heightMU = unitConverter.convertUnits(heightInch, esriUnits.esriInches, pageLayout.getPage().getUnits());
                double widthMU = unitConverter.convertUnits(widthInch, esriUnits.esriInches, pageLayout.getPage().getUnits());
                envelope = new Envelope();
                envelope.putCoords(pt1.getX(), pt1.getY(), pt1.getX() + widthMU, pt1.getY() + heightMU);
                pictureElement.setGeometry(envelope);
            } else if (geom instanceof IEnvelope) {
                envelope = (IEnvelope) geom;
                pictureElement.setGeometry(envelope);
            }
            //LayoutUtils.positionElement(logger, pictureElement, envelope, anchor);
            return pictureElement;
        }
        logger.error(TAG, "pictureTmpFile is null!");
        return null;
    }

    @Override
    public String getType() {
        return TYPE;
    }
}