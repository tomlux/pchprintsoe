/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.etat.pch.gis.utils;

import com.esri.arcgis.system.esriUnits;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * converts between strings and the esriUnits-constants
 *
 * @author abajramovic
 * @author schullto
 */
public class UnitConverter {

    private static final String TAG = "UnitConverter";

    private static Map<String, Integer> nameLookupTable = new HashMap<String, Integer>();
    private static Map<Integer, String> valueLookupTable = new HashMap<Integer, String>();

    static {
        nameLookupTable.put("esriUnits.esriKilometers".toLowerCase(), esriUnits.esriKilometers);
        nameLookupTable.put("esriKilometers".toLowerCase(), esriUnits.esriKilometers);
        nameLookupTable.put("kilometers", esriUnits.esriKilometers);
        nameLookupTable.put("kilometer", esriUnits.esriKilometers);
        nameLookupTable.put("km", esriUnits.esriKilometers);

        nameLookupTable.put("esriUnits.esriCentimeters".toLowerCase(), esriUnits.esriCentimeters);
        nameLookupTable.put("esriCentimeters".toLowerCase(), esriUnits.esriCentimeters);
        nameLookupTable.put("centimeters", esriUnits.esriCentimeters);
        nameLookupTable.put("centimeter", esriUnits.esriCentimeters);
        nameLookupTable.put("cm", esriUnits.esriCentimeters);

        nameLookupTable.put("esriUnits.esriMillimeters".toLowerCase(), esriUnits.esriMillimeters);
        nameLookupTable.put("esriMillimeters".toLowerCase(), esriUnits.esriMillimeters);
        nameLookupTable.put("millimeter", esriUnits.esriMillimeters);
        nameLookupTable.put("mm", esriUnits.esriMillimeters);

        nameLookupTable.put("esriUnits.esriMeters".toLowerCase(), esriUnits.esriMeters);
        nameLookupTable.put("esriMeters".toLowerCase(), esriUnits.esriMeters);
        nameLookupTable.put("meters", esriUnits.esriMeters);
        nameLookupTable.put("meter", esriUnits.esriMeters);
        nameLookupTable.put("m", esriUnits.esriMeters);

        nameLookupTable.put("esriUnits,esriNauticalMiles".toLowerCase(), esriUnits.esriNauticalMiles);
        nameLookupTable.put("esriNauticalMiles".toLowerCase(), esriUnits.esriNauticalMiles);
        nameLookupTable.put("nauticalmiles", esriUnits.esriNauticalMiles);
        nameLookupTable.put("nauticalmile", esriUnits.esriNauticalMiles);
        nameLookupTable.put("nm", esriUnits.esriNauticalMiles);

        nameLookupTable.put("esriUnits.esriMiles".toLowerCase(), esriUnits.esriMiles);
        nameLookupTable.put("esriMiles".toLowerCase(), esriUnits.esriMiles);
        nameLookupTable.put("miles", esriUnits.esriMiles);
        nameLookupTable.put("mile", esriUnits.esriMiles);

        nameLookupTable.put("esriUnits.esriFeet".toLowerCase(), esriUnits.esriFeet);
        nameLookupTable.put("esriFeet".toLowerCase(), esriUnits.esriFeet);
        nameLookupTable.put("feets", esriUnits.esriFeet);
        nameLookupTable.put("feet", esriUnits.esriFeet);
        nameLookupTable.put("ft", esriUnits.esriFeet);

        nameLookupTable.put("esriUnits.esriYards".toLowerCase(), esriUnits.esriYards);
        nameLookupTable.put("esriYards".toLowerCase(), esriUnits.esriYards);
        nameLookupTable.put("yards", esriUnits.esriYards);
        nameLookupTable.put("yard", esriUnits.esriYards);

        nameLookupTable.put("esriUnits.esriInches".toLowerCase(), esriUnits.esriInches);
        nameLookupTable.put("esriInches".toLowerCase(), esriUnits.esriInches);
        nameLookupTable.put("inches", esriUnits.esriInches);
        nameLookupTable.put("inche", esriUnits.esriInches);
        nameLookupTable.put("in", esriUnits.esriInches);

        nameLookupTable.put("esriUnits.esriDecimalDegrees".toLowerCase(), esriUnits.esriDecimalDegrees);
        nameLookupTable.put("esriDecimalDegrees".toLowerCase(), esriUnits.esriDecimalDegrees);
        nameLookupTable.put("degrees", esriUnits.esriDecimalDegrees);
        nameLookupTable.put("degree", esriUnits.esriDecimalDegrees);

        nameLookupTable.put("esriUnits.esriDecimeters".toLowerCase(), esriUnits.esriDecimeters);
        nameLookupTable.put("esriDecimeters".toLowerCase(), esriUnits.esriDecimeters);
        nameLookupTable.put("decimeters", esriUnits.esriDecimeters);
        nameLookupTable.put("decimeter", esriUnits.esriDecimeters);
        nameLookupTable.put("dm", esriUnits.esriDecimeters);

        nameLookupTable.put("esriUnits.esriPoints".toLowerCase(), esriUnits.esriPoints);
        nameLookupTable.put("esriPoints".toLowerCase(), esriUnits.esriPoints);
        nameLookupTable.put("points", esriUnits.esriPoints);
        nameLookupTable.put("point", esriUnits.esriPoints);
        nameLookupTable.put("pt", esriUnits.esriPoints);

        valueLookupTable.put(esriUnits.esriKilometers, "km");
        valueLookupTable.put(esriUnits.esriCentimeters, "cm");
        valueLookupTable.put(esriUnits.esriMillimeters, "mm");
        valueLookupTable.put(esriUnits.esriMeters, "m");
        valueLookupTable.put(esriUnits.esriNauticalMiles, "nm");
        valueLookupTable.put(esriUnits.esriMiles, "miles");
        valueLookupTable.put(esriUnits.esriFeet, "ft");
        valueLookupTable.put(esriUnits.esriYards, "yards");
        valueLookupTable.put(esriUnits.esriInches, "inch");
        valueLookupTable.put(esriUnits.esriDecimalDegrees, "dd");
        valueLookupTable.put(esriUnits.esriDecimeters, "dm");
        valueLookupTable.put(esriUnits.esriPoints, "pt");
    }


    public static String getStringFromEsriUnit(SOELogger logger, Integer unit) {
        if (valueLookupTable.containsKey(unit))
            return valueLookupTable.get(unit);
        logger.error(TAG, "getStringFromEsriUnit: unknown unit", unit);
        return null;
    }

    /**
     * @param unitString kilometers|meters|centimeters|millimeters|miles|feet|years|inches|degrees|nauticalmiles|points
     * @return
     */
    public static int getEsriUnitFromString(SOELogger logger, String unitString) {
        unitString = unitString.toLowerCase();
        if (unitString == null || "".equals(unitString)) {
            return esriUnits.esriUnknownUnits;
        } else if (nameLookupTable.containsKey(unitString)) {
            return nameLookupTable.get(unitString);
        } else {
            try {
                return Integer.parseInt(unitString);
            } catch (NumberFormatException nfEx) {
            }
        }
        logger.error(TAG,"getEsriUnitFromString.UNKNOWN_UNIT",unitString);
        return esriUnits.esriUnknownUnits;
    }

    public static double convertValue(SOELogger logger, int inputUnit, int outputUnit, double value) {
        if (inputUnit == outputUnit) return value;
        try {
            if (valueLookupTable.containsKey(inputUnit) && valueLookupTable.containsKey(outputUnit)) {
                String inputUnitName = valueLookupTable.get(inputUnit);
                String outputUnitName = valueLookupTable.get(outputUnit);
                com.esri.arcgis.system.UnitConverter unitConverter = new com.esri.arcgis.system.UnitConverter();
                double convertedValue = unitConverter.convertUnits(value, inputUnit, outputUnit);
                unitConverter.release();
                logger.debug(TAG, "convertValue: " + value + " " + inputUnitName + "  ==>  " + convertedValue + " " + outputUnitName);
                return convertedValue;
            } else {
                if (!valueLookupTable.containsKey(inputUnit))
                    logger.error(TAG, "calculateScale: unknown units-> inputUnit=", inputUnit);
                if (!valueLookupTable.containsKey(outputUnit))
                    logger.error(TAG, "calculateScale: unknown units-> outputUnit=", outputUnit);
                return Double.NaN;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.error(TAG, "calculateScale: unknown units-> input=" + inputUnit + "    output=" + outputUnit);
        return Double.NaN;
    }
}