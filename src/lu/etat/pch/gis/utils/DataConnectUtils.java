/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.datasourcesGDB.AccessWorkspaceFactory;
import com.esri.arcgis.datasourcesGDB.FileGDBWorkspaceFactory;
import com.esri.arcgis.datasourcesraster.RasterWorkspace;
import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.gisclient.*;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.PropertySet;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 21, 2010
 * Time: 6:17:21 PM
 */
public class DataConnectUtils {
    public static void main(String[] args) {
        try {
            LicenceUtils.initArcEngine();
            DataConnectUtils dc = new DataConnectUtils();
            //createWMTSLayer("http://antares02.pch.etat.lu/erdas-iws/ogc/wmts/GeoServices", (short) 0);
            //createWMTSLayer("http://antares01.pch.etat.lu/erdas-iws/ogc/wmts/APOLLO-Catalog", (short) 0);
            dc.createWMTSLayer("http://antares01.pch.etat.lu/erdas-iws/ogc/wmts/WMTS_ACT_ortho?", "2010_ortho_ACT.ecw", (short) 0);

            /*
            IActiveView activeView = null;
            String folderName = "d:\\";
            String rasterName ="flowDirection.tif";
            RasterWorkspaceFactory rasterWorkspaceFactory = new RasterWorkspaceFactory();
            RasterWorkspace inRasterWorkspace = new RasterWorkspace(rasterWorkspaceFactory.openFromFile(folderName, 0));
            RasterDataset inRasterDataset1 = (RasterDataset) inRasterWorkspace.openRasterDataset(rasterName);
            System.out.println("inRasterDataset1.getRasterInfo().getWidth() = " + inRasterDataset1.getRasterInfo().getWidth());
            System.out.println("inRasterDataset1.getRasterInfo().getHeight() = " + inRasterDataset1.getRasterInfo().getHeight());
            RasterLayer rasterLayer = new RasterLayer();
            rasterLayer.createFromDataset(inRasterDataset1);
            rasterLayer.setName("myLoadedRaster");
            activeView.getFocusMap().addLayer(rasterLayer);
            activeView.refresh();

            if (1 == 1) System.exit(0);
            FeatureClass fc = FeatureClassUtils.createFeatureClassFromArcSDEBasedDataSource("localhost", "5151", "sde", "sde", "sde", "DEFAULT", "dem5m2003");
            System.out.println("fc.getShapeFieldName() = " + fc.getShapeFieldName());
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ILayer createFileGeoDataBaseLayer(String geoDB, String featureClassName) throws IOException {
        FileGDBWorkspaceFactory fGDB = new FileGDBWorkspaceFactory();
        Workspace workspace = new Workspace(fGDB.openFromFile(geoDB, 0));
        FeatureClass fClass = new FeatureClass(workspace.openFeatureClass(featureClassName));
        FeatureLayer fLayer = new FeatureLayer();
        fLayer.setName(featureClassName);
        fLayer.setFeatureClassByRef(fClass);
        return fLayer;
    }

    public ILayer createAccessGeoDataBaseLayer(String geoDB, String featureClassName) throws IOException {
        AccessWorkspaceFactory awFact = new AccessWorkspaceFactory();
        Workspace workspace = new Workspace(awFact.openFromFile(geoDB, 0));
        FeatureClass fClass = new FeatureClass(workspace.openFeatureClass(featureClassName));
        FeatureLayer fLayer = new FeatureLayer();
        fLayer.setName(featureClassName);
        fLayer.setFeatureClassByRef(fClass);
        return fLayer;
    }

    public void printOutPropSet(IPropertySet propSet) {
        Object[] a = new Object[1];
        Object[] b = new Object[1];
        try {
            propSet.getAllProperties(a, b);
            Object[] aa = (Object[]) a[0];
            Object[] bb = (Object[]) b[0];
            for (int i = 0; i < bb.length; i++) {
                System.out.println(aa[i] + " = " + bb[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //todo: make a simpler AGS connection 

    public ILayer createArcGISService(String url, String mapService) throws IOException {
        //Set up connection
        IAGSServerConnectionFactory connectionFactory = new AGSServerConnectionFactory();
        IPropertySet propertySet = new PropertySet();
        IAGSServerConnection connection;
        propertySet.setProperty("url", url);
        connection = connectionFactory.open(propertySet, 0);


        IAGSEnumServerObjectName serverObjectNames = connection.getServerObjectNames();
        serverObjectNames.reset();

        IAGSServerObjectName serverObjectName;
        while ((serverObjectName = serverObjectNames.next()) != null) {
            if (serverObjectName.getName().equals(mapService) && serverObjectName.getType().equals("MapServer")) {
                if (serverObjectName instanceof AGSServerObjectName) {
                    AGSServerObjectName name = (AGSServerObjectName) serverObjectName;
                    IPropertySet propertySet2 = name.getAGSServerConnectionName().getConnectionProperties();
                    Object[] a = new Object[propertySet.getCount()], b = new Object[propertySet.getCount()];
                    propertySet2.getAllProperties(a, b);
                    IMapServer mapServer = (IMapServer) name.open();
                    MapServerLayer layer = new MapServerLayer();
                    layer.serverConnect(serverObjectName, mapServer.getDefaultMapName());
                    return layer;
                }
            }
        }
        return null;
    }

    public void testAGS() throws IOException {
        MapServerLayer mapServerLayer = new MapServerLayer();
        AGSServerConnectionName connName = new AGSServerConnectionName();
        PropertySet propSet = new PropertySet();
        propSet.setProperty("url", "http://localhost:8399/arcgis/services");
        propSet.setProperty("HTTPTIMEOUT", 60);
        propSet.setProperty("MESSAGEFORMAT", 2);
        propSet.setProperty("TOKENSERVICEURL", "http://babifer:8399/arcgis/tokens");
        connName.setConnectionProperties(propSet);
        connName.open();
        mapServerLayer.connect(connName);
    }

    public WMSMapLayer createWMSLayer(String wmsURL, short agsAlpha) throws IOException {
        WMSMapLayer wmsMapLayer = new WMSMapLayer();
        WMSConnectionName connName = new WMSConnectionName();
        IPropertySet propSet = new PropertySet();
        if (wmsURL.indexOf("/MapServer/WMSServer") > 0) {
            wmsURL = wmsURL.substring(0, wmsURL.indexOf("/MapServer/WMSServer") + 20);
        }
        propSet.setProperty("URL", wmsURL);
        propSet.setProperty("version", "1.3.0");
        connName.setConnectionProperties(propSet);
        wmsMapLayer.connect(connName);
        for (int i = 0; i < wmsMapLayer.getCount(); i++) {
            ILayer layer = wmsMapLayer.getLayer(i);
            SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
            IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
            ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
            layer.setSpatialReferenceByRef(spatialReference);
            layer.setVisible(true);
            if (layer instanceof WMSGroupLayer) {
                WMSGroupLayer wmsGroupLayer = (WMSGroupLayer) layer;
                for (int j = 0; j < wmsGroupLayer.getCount(); j++) {
                    ILayer childLayer = wmsGroupLayer.getLayer(j);
                    childLayer.setVisible(true);
                }
            }
        }
        wmsMapLayer.setTransparency(agsAlpha);
        return wmsMapLayer;
    }

    public WMTSLayer createWMTSLayer(String wmtsURL, String layerName, short agsAlpha) throws IOException {
        System.out.println("wmtsURL = " + wmtsURL);
        WMTSLayer wmtsLayer = new WMTSLayer();
        WMTSConnectionName connName = new WMTSConnectionName();
        IPropertySet propSet = new PropertySet();
        propSet.setProperty("URL", wmtsURL);
        if (layerName != null && layerName.trim().length() > 0) {
            propSet.setProperty("LAYERNAME", layerName);
        }
        propSet.setProperty("version", "1.0.0");
        connName.setConnectionProperties(propSet);
        wmtsLayer.connect(connName);
        wmtsLayer.setTransparency(agsAlpha);

        WMTSConnection wmtsConnection = (WMTSConnection) connName.openEx(null);

        for (int i = 0; i < wmtsConnection.getLayerDescriptionCount(); i++) {
            WMTSLayerDescription wmtsLayerDescription = (WMTSLayerDescription) wmtsConnection.getLayerDescription(i);
            System.out.println("wmtsLayerDescription[" + i + "] = " + wmtsLayerDescription.getTitle());
            System.out.println("    layerDescription[" + i + "] = " + wmtsLayerDescription.getIdentifier());
        }
        IWorkspace ws = wmtsLayer.getWorkspace();
        System.out.println("ws = " + ws);
        RasterWorkspace rasterWorkspace = (RasterWorkspace) ws;
        System.out.println("rasterWorkspace = " + rasterWorkspace);
        System.out.println("rasterWorkspace.name = " + rasterWorkspace.getName());
        IEnumDataset enumDataSet = rasterWorkspace.getDatasets(0);
        System.out.println("enumDataSet = " + enumDataSet);
        IDataset dataset = null;
        while ((dataset = enumDataSet.next()) != null) {
            System.out.println("dataset = " + dataset);
            System.out.println("dataset.getName() = " + dataset.getName());
        }
        System.out.println("wmtsLayer = " + wmtsLayer.getLayerName());
        return wmtsLayer;
    }

    public ISpatialReference getSpatialReferenceLuxembourg() throws IOException {
        SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
        IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
        ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
        return spatialReference;
    }
}
