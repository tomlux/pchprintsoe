/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.beans.pk;

import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 11, 2010
 * Time: 1:55:01 PM
 */
public class PKResultBean {
    private String route;
    private double pk;
    private double offset;
    private double x;
    private double y;

    public PKResultBean() {
    }

    public PKResultBean(String route, double pk, double offset, double x, double y) {
        this.route = route;
        this.pk = pk;
        this.offset = offset;
        this.x = x;
        this.y = y;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public double getPk() {
        return pk;
    }

    public void setPk(double pk) {
        this.pk = pk;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "PKResultBean{" +
                "route='" + route + '\'' +
                ", pk=" + pk +
                ", offset=" + offset +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("route", route);
        obj.put("pk", pk);
        obj.put("offset", offset);
        obj.put("x", x);
        obj.put("y", y);
        return obj;
    }
}
