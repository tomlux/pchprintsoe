/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.beans.pk;

import javax.swing.table.DefaultTableModel;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 20, 2010
 * Time: 9:36:23 PM
 */
public class PKResultsTableModel extends DefaultTableModel {
    String[] tableColumnNames = new String[]{"Route", "PK", "Offset", "X", "Y"};

    public PKResultsTableModel(List<PKResultBean> pkResults) {
        setColumnIdentifiers(tableColumnNames);
        for (PKResultBean pkResultBean : pkResults) {
            Object[] objects = new Object[5];
            objects[0] = pkResultBean.getRoute();
            objects[1] = pkResultBean.getPk();
            objects[2] = pkResultBean.getOffset();
            objects[3] = pkResultBean.getX();
            objects[4] = pkResultBean.getY();
            addRow(objects);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
