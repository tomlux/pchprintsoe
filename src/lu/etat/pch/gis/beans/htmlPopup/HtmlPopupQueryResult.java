/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.beans.htmlPopup;

import com.esri.arcgis.carto.FeatureLayer;
import com.esri.arcgis.geodatabase.IFeature;
import com.esri.arcgis.geodatabase.IField;
import com.esri.arcgis.geodatabase.IFields;
import com.esri.arcgis.geometry.ISpatialReference;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;

import java.io.IOException;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 13, 2010
 * Time: 10:20:46 AM
 */
public class HtmlPopupQueryResult {
    private String displayName;
    private Map<String, String> fieldAliases;
    private int geometryType; //look at com.esri.arcgis.geometry.esriGeometryType
    private ISpatialReference spatialReference;
    private List<HtmlPopupQueryFeature> features;
    private String oidFieldName;
    private String[] outFieldNames;
    private boolean returnGeometry;

    public HtmlPopupQueryResult(FeatureLayer featureLayer, String[] outFieldNames, boolean returnGeometry) {
        if (featureLayer == null) return;
        try {
            this.outFieldNames = outFieldNames;
            this.returnGeometry = returnGeometry;
            displayName = featureLayer.getDisplayField();
            IFields fields = featureLayer.getFields();
            fieldAliases = new HashMap<String, String>();
            for (int i = 0; i < fields.getFieldCount(); i++) {
                IField field = fields.getField(i);
                if (outFieldNames != null && outFieldNames.length > 0) {
                    if (field.getGeometryDef() == null) {
                        if (Arrays.binarySearch(outFieldNames, field.getName().toUpperCase()) >= 0) {
                            fieldAliases.put(field.getName(), field.getAliasName());
                        }
                    }
                } else {
                    fieldAliases.put(field.getName(), field.getAliasName());
                }
            }
            geometryType = featureLayer.getFeatureClass().getShapeType();
            spatialReference = featureLayer.getSpatialReference();
            features = new ArrayList<HtmlPopupQueryFeature>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addFeature(IFeature feature, String htmlPopup, boolean returnGeometry, String  htmlPopupUrl) throws IOException {
        Map<String, Object> attributes = new HashMap<String, Object>();
        IFields fields = feature.getFields();
        for (int i = 0; i < fields.getFieldCount(); i++) {
            IField field = fields.getField(i);
            if (field.getGeometryDef() == null) {
                String fieldName = field.getName();
                Object fieldValue = feature.getValue(fields.findField(fieldName));
                if (fieldValue != null) {
                    attributes.put(fieldName, fieldValue);
                }
            }
        }
        HtmlPopupQueryFeature htmlPopupQueryFeature = new HtmlPopupQueryFeature(attributes, null,htmlPopup,htmlPopupUrl);
        if (returnGeometry) {
            htmlPopupQueryFeature.setGeometry(feature.getShape());
        }
        features.add(htmlPopupQueryFeature);
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Map<String, String> getFieldAliases() {
        return fieldAliases;
    }

    public void setFieldAliases(Map<String, String> fieldAliases) {
        this.fieldAliases = fieldAliases;
    }

    public int getGeometryType() {
        return geometryType;
    }

    public void setGeometryType(int geometryType) {
        this.geometryType = geometryType;
    }

    public ISpatialReference getSpatialReference() {
        return spatialReference;
    }

    public void setSpatialReference(ISpatialReference spatialReference) {
        this.spatialReference = spatialReference;
    }

    public List<HtmlPopupQueryFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<HtmlPopupQueryFeature> features) {
        this.features = features;
    }

    public JSONObject toJSON() throws JSONException, IOException {
        JSONObject jsonObj = new JSONObject();
        JSONArray fieldAliasesArr = new JSONArray();
        jsonObj.put("fieldAliases", fieldAliasesArr);
        JSONArray featuresArr = new JSONArray();
        jsonObj.put("features", featuresArr);

        jsonObj.put("displayFieldName", displayName);
        Set<Map.Entry<String, String>> fieldAliasesSet = fieldAliases.entrySet();
        for (Map.Entry<String, String> fieldAliasEntry : fieldAliasesSet) {
            JSONObject fieldAliasObj = new JSONObject();
            fieldAliasObj.put(fieldAliasEntry.getKey(), fieldAliasEntry.getValue());
            fieldAliasesArr.put(fieldAliasObj);
        }
        if (returnGeometry) {
            jsonObj.put("geometryType", AgsJsonGeometry.convertGeometryTypeToJsonString(geometryType));
            jsonObj.put("spatialReference", AgsJsonGeometry.convertSpatialReferenceToJSON(spatialReference));
        }
        for (HtmlPopupQueryFeature htmlPopupQueryFeature : features) {
            featuresArr.put(htmlPopupQueryFeature.toJSON());
        }

        return jsonObj;
    }
    /*
    {
"displayFieldName" : "AREANAME",
"fieldAliases" : {
    "ST" : "ST",
    "POP2000" : "Population - 2000",
    "AREANAME" : "City Name"
},
"geometryType" : "esriGeometryPoint",
"spatialReference" : {"wkid" : 4326},
"features" : [
    {
    "attributes" : {
        "ST" : "CA",
        "POP2000" : 3694820,
        "AREANAME" : "Los Angeles"
    },
    "geometry" : { "x" : -118.37, "y" : 34.086 }
    },
    {
    "attributes" : {
        "ST" : "CA",
        "POP2000" : 461522,
        "AREANAME" : "Long Beach"
    },
    "geometry" : { "x" : -118.15, "y" : 33.80 }
    }
]
}
     */

    public String getOidFieldName() {
        return oidFieldName;
    }

    public void setOidFieldName(String oidFieldName) {
        this.oidFieldName = oidFieldName;
    }

}
