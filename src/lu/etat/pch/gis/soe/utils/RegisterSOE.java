package lu.etat.pch.gis.soe.utils;

import com.esri.arcgis.interop.extn.ServerObjectExtProperties;
import com.esri.arcgis.server.*;
import com.esri.arcgis.system.AoInitialize;
import com.esri.arcgis.system.EngineInitializer;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.ServerInitializer;
import lu.etat.pch.gis.soe.PCHExportSOE;
import lu.etat.pch.gis.soe.print.PCHPrintSOE;
import lu.etat.pch.gis.soe.print.PrintGUI;
import lu.etat.pch.gis.utils.LicenceUtils;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/8/12
 * Time: 5:54 AM
 */
public class RegisterSOE {
    private final String soType = "MapServer";

    public static void main(String[] args) {
        if (args.length >= 4) {
            String server = args[0];
            String domain = args[1];
            String user = args[2];
            String pass = args[3];
            System.out.println("server = " + server);
            System.out.println("domain = " + domain);
            System.out.println("user = " + user);
            System.out.println("pass = " + pass);
            boolean unregister = false;
            Class registerClass = PCHPrintSOE.class;
            if (args.length >= 5 && args[4].equalsIgnoreCase("unregister")) {
                unregister = true;
                if (args.length >= 6) {
                    String toUnRegisterClassName = args[5];
                    if (toUnRegisterClassName.equalsIgnoreCase("PCHExportSOE")) registerClass = PCHExportSOE.class;
                    /*
                    else if (toUnRegisterClassName.equalsIgnoreCase("VirtualDirSOE"))
                        registerClass = VirtualDirSOE.class;
                        */
                    else if (toUnRegisterClassName.equalsIgnoreCase("PCHPrintSOE")) registerClass = PCHPrintSOE.class;
                    else {
                        System.err.println("Can't find class '" + toUnRegisterClassName + "' to perform unregistration!");
                        System.exit(0);
                    }
                }
            }
            RegisterSOE registerSOE = new RegisterSOE();
            if (unregister) {
                registerSOE.unregister(server, domain, user, pass, registerClass);
            } else {
                registerSOE.register(server, domain, user, pass, registerClass);
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("printgui")) {
            startPrintGUI();
        } else {
            System.err.println("..\\..\\jre\\bin\\java -jar PCHarcgisSOE.jar server domain user pass <unregister> <toUnregisterSOEName>");
        }

    }

    private static void startPrintGUI() {
        PrintGUI printGUI = new PrintGUI();
        try {
            AoInitialize aoInit = LicenceUtils.initArcEngine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFrame myFrame = new JFrame("PchPrintSOE tester");
        myFrame.getContentPane().add(printGUI.mainPanel);
        myFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        myFrame.setSize(700, 600);
        myFrame.setVisible(true);
        System.out.println("myFrame = " + myFrame);
    }

    private void unregister(String server, String domain, String user, String pass, Class soeClass) {
        System.out.println("RegisterSOE.unregister...");
        ServerObjectAdmin soa = initSOA(server, domain, user, pass);
        if (soa == null) {
            System.err.println("ERROR connecting to the AGS server");
            return;
        }
        String soeFullyQualifiedName = soeClass.getPackage().getName() + "." + soeClass.getSimpleName();
        String soeName = soeFullyQualifiedName.substring(soeFullyQualifiedName.lastIndexOf(".") + 1, soeFullyQualifiedName.length());
        System.out.println("Trying to unregister the SOE: " + soeFullyQualifiedName);
        try {
            boolean soeAlreadyRegisterd = alreadyRegistered(soa, soeName);
            if (!soeAlreadyRegisterd) {
                System.err.println("SOE isn't yet registered!!");
                System.exit(0);
            }
            soa.deleteExtensionType(soType, soeName);
            System.out.println("SOE unregistered.");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void register(String server, String domain, String user, String pass, Class soeClass) {
        System.out.println("RegisterSOE.register...");
        try {
            ServerObjectAdmin soa = initSOA(server, domain, user, pass);
            if (soa == null) {
                System.err.println("ERROR connecting to the AGS server");
                return;
            }

            String soeFullyQualifiedName = soeClass.getPackage().getName() + "." + soeClass.getSimpleName();
            String soeName = soeFullyQualifiedName.substring(soeFullyQualifiedName.lastIndexOf(".") + 1, soeFullyQualifiedName.length());
            System.out.println("Trying to register the SOE: " + soeFullyQualifiedName);

            boolean soeAlreadyRegisterd = alreadyRegistered(soa, soeName);
            if (soeAlreadyRegisterd) {
                System.out.println("Your SOE '" + soeName + "' is already registered!");
                System.exit(0);
            }

            if (soeClass.isAnnotationPresent(ServerObjectExtProperties.class)) {
                ServerObjectExtProperties annotation = (ServerObjectExtProperties) soeClass.getAnnotation(ServerObjectExtProperties.class);
                IServerObjectExtensionType3 serverObjectExtensionType = new IServerObjectExtensionType3Proxy(soa.createExtensionType());
                System.out.println("soeName = " + soeName);
                serverObjectExtensionType.setName(soeName);
                String displayName = annotation.displayName();
                System.out.println("displayName = " + displayName);
                if (!displayName.isEmpty()) {
                    serverObjectExtensionType.setDisplayName(displayName);
                } else {
                    serverObjectExtensionType.setDisplayName(soeName);
                }
                serverObjectExtensionType.setCLSID(soeFullyQualifiedName);
                serverObjectExtensionType.setDescription(annotation.description());

                IPropertySet propSet = serverObjectExtensionType.getProperties();
                propSet.setProperty("AllWebCapabilities", "printMap,printLayout");
                propSet.setProperty("DefaultWebCapabilities", "");
                serverObjectExtensionType.setPropertiesByRef(propSet);

                IPropertySet infoPropSet = serverObjectExtensionType.getInfo();
                infoPropSet.setProperty("SupportsREST", "true");
                infoPropSet.setProperty("SupportsMSD", "true");
                serverObjectExtensionType.setInfoByRef(infoPropSet);

                System.out.println("trying to add SOE...");
                soa.addExtensionType(soType, serverObjectExtensionType);//soType is "MapServer"
                System.out.println("... SOE successfully added :-)");
                soa.release();
            } else {
                throw new Exception("\nUnable to add Server Object Extension \"" + soeFullyQualifiedName + "\" to service of type " + soType + ".\n" +
                        "Reason: The \"ServerObjectExtProperties\" annotation containing properties and operations info is missing from " +
                        "Server Object Extension \"" + soeFullyQualifiedName + ".java\".\nAll Java SOE's at ArcGIS 10 must be annotated with this annotation.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("RegisterSOE.register.DONE");
    }

    private ServerObjectAdmin initSOA(String server, String domain, String user, String pass) {
        try {
            EngineInitializer.initializeVisualBeans();
            LicenceUtils.initArcEngine();

            ServerInitializer serverInitializer = new ServerInitializer();
            serverInitializer.initializeServer(domain, user, pass);
            ServerConnection serverConnection = new ServerConnection();
            serverConnection.connect(server);
            //IServerObjectManager som = serverConnection.getServerObjectManager();
            //IServerObjectAdmin serverObjectAdmin = serverConnection.getServerObjectAdmin();
            ServerObjectAdmin soa = new ServerObjectAdmin(serverConnection.getServerObjectAdmin());
            return soa;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean alreadyRegistered(ServerObjectAdmin soa, String soeName) throws IOException {
        IEnumServerObjectExtensionType enumServerObjectExtensionType = soa.getExtensionTypes(soType);
        enumServerObjectExtensionType.reset();
        IServerObjectExtensionType extensionType = enumServerObjectExtensionType.next();
        System.out.println("Current SOEs:");
        boolean soeAlreadyRegisterd = false;
        while (extensionType != null) {
            System.out.println("\t" + extensionType.getName());
            if (soeName.equals(extensionType.getName())) {
                soeAlreadyRegisterd = true;
            }
            extensionType = enumServerObjectExtensionType.next();
        }
        System.out.println("..... current SOE");
        return soeAlreadyRegisterd;
    }
}
