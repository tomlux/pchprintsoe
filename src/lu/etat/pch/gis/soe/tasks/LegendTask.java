/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 6, 2010
 * Time: 6:05:36 PM
 */
public class LegendTask extends AbstractTask {
    private static final String TAG = "LegendTask";
    private static String legendFileExtension = ".png";
    private File legendFileDirectory;

    public LegendTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        try {
            legendFileDirectory = getPhysicalMapServiceOutputFolder("legend");
        } catch (IOException ex) {
            logger.error(TAG, "ExportLayerTask().layerFileDirectory-ERROR", ex);
        }
    }

    public String exportLayers() {
        String filePath;
        StringBuffer sb = new StringBuffer();
        try {
            filePath = mapServer.getFilePath();
            logger.debug(TAG, "Got configFileName from mapServer", filePath);
            if (filePath == null || filePath.trim().length() == 0) {
                logger.warning(TAG, "FilePath not found in mapserver.Reading configuration file.");
                String configFileName = "C:\\Program Files\\ArcGIS\\server\\user\\cfg\\" + mapServer.getConfigurationName() + ".MapServer.cfg";
                File configFile = new File(configFileName);
                if (!configFile.canRead()) {
                    configFileName = "/data/arcgis/server9.4/server/user/cfg";
                    configFile = new File(configFileName);
                }
                if (configFile.canRead()) {
                    FileReader fileReader = new FileReader(configFile);
                    BufferedReader buffReader = new BufferedReader(fileReader);
                    String configLine;
                    filePath = null;
                    while ((configLine = buffReader.readLine()) != null) {
                        if (configLine.indexOf("<FilePath>") > 0) {
                            filePath = configLine.substring(configLine.indexOf("<FilePath>") + 10);
                            filePath = filePath.substring(0, filePath.indexOf("</FilePath>"));
                            logger.debug(TAG, "FilePath found in coniguration file", filePath);
                            break;
                        }
                    }
                    fileReader.close();
                }
            }
            if (filePath != null) {
                if (filePath.endsWith(".msd")) {
                    filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                    logger.warning(TAG, "Reading mxd file instead of msd file", filePath);
                }
                MapReader mapReader = new MapReader();
                logger.debug("Using FilePath", filePath);
                mapReader.open(filePath);
                IMap map = mapReader.getMap(0);
                logger.debug(TAG, "open.getMap", map.getName());
                for (int i = 0; i < map.getLayerCount(); i++) {
                    ILayer myLayer = map.getLayer(i);
                    myLayer.setVisible(true);
                    logger.debug(TAG, "gotLayer", myLayer);

                    ImageType imageType2 = new ImageType();
                    imageType2.setFormat(esriImageFormat.esriImagePNG);
                    imageType2.setReturnType(esriImageReturnType.esriImageReturnMimeData);
                    ImageDisplay imageDisplay = new ImageDisplay();
                    imageDisplay.setHeight(500);
                    imageDisplay.setWidth(500);
                    imageDisplay.setDeviceResolution(96);
                    ImageDescription imageDesc = new ImageDescription();
                    imageDesc.setType(imageType2);
                    imageDesc.setDisplay(imageDisplay);
                    IMapDescription mapDescription = mapServer.getDefaultPageDescription().getMapFrames().getElement(0).getMapDescription();
                    ILayerDescriptions layerDescriptions = mapDescription.getLayerDescriptions();
                    for (int j = 0; j < layerDescriptions.getCount(); j++) {
                        ILayerDescription layerDescription = layerDescriptions.getElement(j);
                        logger.debug(TAG, "setVisibleFALSE: " + i + " " + j + " " + layerDescription.getID());
                        layerDescription.setVisible(false);
                        if (i == j) {
                            layerDescription.setVisible(true);
                            logger.debug(TAG, "setVisibleTRUE: " + i + " " + j + " " + layerDescription.getID());
                        }
                    }
                    mapDescription.setLayerDescriptions(layerDescriptions);

                    UID uid = new UID();
                    uid.setValue(Legend.class);
                    ILegend legend = (ILegend) map.createMapSurround(uid, null);
                    legend.setTitle("");

                    for (int j = 0; j < layerDescriptions.getCount(); j++) {
                        // ILegendItem legendItm = legend.getItem(j);
                        legend.removeItem(i);
                    }
                    HorizontalBarLegendItem legendItem = new HorizontalBarLegendItem();
                    legendItem.setLayerByRef(myLayer);
                    legendItem.setShowLayerName(false);
                    legendItem.setShowHeading(false);
                    legend.addItem(legendItem);

                    IImageResult imageResult = mapServer.exportLegend(legend, mapDescription, imageDisplay, null, imageDesc);
                    byte[] bytes = imageResult.getMimeData();


                    if (mapServiceName.indexOf("/") > 0) {
                        String mapServiceFolder = mapServiceName.substring(0, mapServiceName.indexOf("/"));
                        logger.debug(TAG, "mapServiceFolder found", mapServiceFolder);
                        File mapServiceFolderDirectory = new File(legendFileDirectory, mapServiceFolder);
                        if (!mapServiceFolderDirectory.exists()) {
                            if (mapServiceFolderDirectory.mkdir())
                                logger.debug(TAG, "directory created", mapServiceFolderDirectory);
                            else
                                logger.error(TAG, "directory NOT created", mapServiceFolderDirectory);
                        }
                    }

                    File legendFilesDirectory = new File(legendFileDirectory, mapServiceName);
                    if (!legendFilesDirectory.exists()) {
                        if (legendFilesDirectory.mkdir())
                            logger.debug(TAG, "directory created", legendFilesDirectory);
                        else
                            logger.error(TAG, "directory NOT created", legendFilesDirectory);
                    }
                    String legendFilename = legendFilesDirectory + File.separator + myLayer.getName() + legendFileExtension;
                    //String legendFilename = legendFileDirectory + File.separator + myLayer.getName() + ".png";
                    logger.debug(TAG, "trying to write legendFilename", legendFilename);
                    File layerLegendImageFile = new File(legendFilename);
                    FileOutputStream fileOutputStream = new FileOutputStream(layerLegendImageFile);
                    fileOutputStream.write(bytes);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    logger.debug(TAG, "legendFilename written", legendFilename);

                }
                mapReader.close();
            }
        } catch (AutomationException e) {
            logger.error(TAG, "exportLayers", e);
            sb.append(e.getMessage());
        } catch (IOException e) {
            logger.error(TAG, "exportLayers", e);
            sb.append(e.getMessage());
        }
        sb.append("new");
        return sb.toString();
    }

}
