/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.analyst3d.RasterSurface;
import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.carto.RasterLayer;
import com.esri.arcgis.geometry.IPolyline;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polyline;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 11, 2010
 * Time: 7:09:11 PM
 */
public class HeightTask extends AbstractTask {
    private static final String TAG = "HeightTask";
    private int demLayerIndex = 1;

    public HeightTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }

    public double getHeightForPoint(double coordX, double coordY) throws IOException {
        Point searchPoint = new Point();
        searchPoint.setX(coordX);
        searchPoint.setY(coordY);
        logger.debug(TAG, "Got searchPoint", searchPoint);
        ILayer layer = map.getLayer(demLayerIndex);
        RasterLayer rLayer = (RasterLayer) layer;
        logger.debug(TAG, "Got rasterLayer", rLayer);
        RasterSurface rasterSurface = new RasterSurface();
        rasterSurface.putRaster(rLayer.getRaster(), 0);
        double height = rasterSurface.getElevation(searchPoint);
        logger.debug(TAG, "Got height", height);
        if (Double.isNaN(height)) return 0;
        return height;
    }

    public IPolyline getHeightForLine(IPolyline polyline, int stepValue) throws IOException {
        logger.debug(TAG, "getHeightForLine.inputLine.length", polyline.getLength());
        ILayer layer = map.getLayer(demLayerIndex);
        if (!layer.isValid()) {
            logger.error(TAG, "Layer '" + layer.getName() + "' is not valid");
            return null;
        }
        RasterLayer rLayer = (RasterLayer) layer;
        logger.debug(TAG, "Got rasterLayer", rLayer);
        RasterSurface rasterSurface = new RasterSurface();
        Polyline[] retGeometry = new Polyline[1];
        logger.debug(TAG, "Got stepValue", stepValue);
        rasterSurface.putRaster(rLayer.getRaster(), 0);
        rasterSurface.interpolateShape(polyline, retGeometry, stepValue);
        logger.debug(TAG, "retGeometry[0]", retGeometry[0]);
        if (retGeometry[0] != null && retGeometry[0] instanceof Polyline) {
            return retGeometry[0];
        }
        return null;
    }

}
