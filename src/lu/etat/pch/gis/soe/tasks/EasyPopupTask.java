/*
 *
 *  *  PCHPrintSOE - Advanced printing SOE for ArcGIS Server
 *  *  Copyright (C) 2010-2012 Tom Schuller
 *  *
 *  *  This program is free software: you can redistribute it and/or modify
 *  *  it under the terms of the GNU Lesser General Public License as published by
 *  *  the Free Software Foundation, either version 3 of the License, or
 *  *  (at your option) any later version.
 *  *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU Lesser General Public License for more details.
 *  *
 *  *  You should have received a copy of the GNU Lesser General Public License
 *  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.NumericFormat;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: schullto
 * Date: 10/18/12
 * Time: 6:17 AM
 */
public class EasyPopupTask extends AbstractTask {
    private static final String TAG = "EasyPopupTask";
    private HashMap<Integer, String> popupLayerIdMap = new HashMap<Integer, String>();

    public EasyPopupTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }

    public String configXML(String layerIds) {
/*
<layer label="Demographics" type="tiled" visible="false" alpha="0.5"
    url="http://server.arcgisonline.com/ArcGIS/rest/services/Demographics/USA_Median_Household_Income/MapServer">
    <sublayer id="1" popupconfig="popups/PopUp_Demographics_BlockGroups.xml"/>
    <sublayer id="2" popupconfig="popups/PopUp_Demographics_Tracts.xml"/>
    <sublayer id="3" popupconfig="popups/PopUp_Demographics_Counties.xml"/>
    <sublayer id="4" popupconfig="popups/PopUp_Demographics_States.xml"/>
</layer>
 */
        StringBuffer retBuffer = new StringBuffer();
        try {
            String mxdFilePath = getMapServiceMxdFile();
            if (mxdFilePath == null) {
                logger.error(TAG, "exportLayers.mxdFilePath.isNULL !!");
                return retBuffer.toString();
            }
            MapReader mapReader = new MapReader();
            mapReader.open(mxdFilePath);
            IMap map = mapReader.getMap(0);
            if (map.getLayerCount() > 0) {
                logger.debug(TAG, "exportLayers.mapName: " + map.getName());
                logger.debug(TAG, "exportLayers.layerCount: " + map.getLayerCount());
                List<ILayer> layerList = getRestIdLayerList(map);

                for (int id = 0; id < layerList.size(); id++) {
                    ILayer layer = layerList.get(id);
                    if (layer instanceof ICompositeLayer) {
                        //ignore for popup
                        retBuffer.append("<!-- layer[" + id + "] is a grouplayer, ignoring it! --> ");
                    } else {
                        retBuffer.append("<sublayer id=\"" + id + "\" popupconfig=\"layerPopUp/layer_" + id + ".xml\"/>");
                    }

                }
            } else {
                logger.debug(TAG, "exportLayers: No layers found, nothing to do.");
            }
            mapReader.close();

        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retBuffer.toString();
    }

    private List<ILayer> getRestIdLayerList(IMap map) throws IOException {
        List<ILayer> layerList = new ArrayList<ILayer>();
        for (int i = 0; i < map.getLayerCount(); i++) {
            ILayer layer = map.getLayer(i);
            layerList.add(layer);
            if (layer instanceof ICompositeLayer) {
                layerList.addAll(getLayerList((ICompositeLayer) layer));
            }
        }
        return layerList;
    }

    private Collection<? extends ILayer> getLayerList(ICompositeLayer layer) throws IOException {
        List<ILayer> layerList = new ArrayList<ILayer>();
        for (int i = 0; i < layer.getCount(); i++) {
            ILayer subLayer = layer.getLayer(i);
            layerList.add(subLayer);
            if (subLayer instanceof ICompositeLayer) {
                layerList.addAll(getLayerList((ICompositeLayer) subLayer));
            }
        }
        return layerList;
    }

    public String layerPopUp(Integer layerId) {
        if (!popupLayerIdMap.containsKey(layerId)) {
            String popup = calculatePopup(layerId);
            popupLayerIdMap.put(layerId, popup);
            return popup;
        }
        return popupLayerIdMap.get(layerId);
    }


    private String calculatePopup(Integer layerId) {
        StringBuilder retBuffer = new StringBuilder();
        try {
            String mxdFilePath = getMapServiceMxdFile();
            if (mxdFilePath == null) {
                logger.error(TAG, "calculatePopup.mxdFilePath.isNULL !!");
                return retBuffer.toString();
            }
            MapReader mapReader = new MapReader();
            mapReader.open(mxdFilePath);
            IMap map = mapReader.getMap(0);
            if (map.getLayerCount() > 0) {
                logger.debug(TAG, "calculatePopup.mapName", map.getName());
                logger.debug(TAG, "calculatePopup.layerCount", map.getLayerCount());
                List<ILayer> layerList = getRestIdLayerList(map);

                ILayer layer = layerList.get(layerId);
                //System.out.println("layer.getName() = " + layer.getName());
                if (layer instanceof FeatureLayer) {
                    FeatureLayer featureLayer = (FeatureLayer) layer;
                    logger.debug(TAG, "calculatePopup.featureLayer", featureLayer.getName());
                    retBuffer.append("");
                    String displayField = "??";
                    if (featureLayer.getExpressionProperties() != null
                            && featureLayer.getExpressionProperties().getExpression() != null) {
                        String expr = featureLayer.getExpressionProperties().getExpression();
                        expr = expr.replace(" & ", "");
                        expr = expr.replace("&", "");
                        expr = expr.replace("{", "");
                        expr = expr.replace("}", "");
                        expr = expr.replace("[", "{");
                        expr = expr.replace("]", "}");
                        expr = expr.replace("\"", "");
                        displayField = expr;
                    } else {
                        displayField = "{" + featureLayer.getDisplayField() + "}";
                    }
                    retBuffer.append("<?xml version=\"1.0\" ?>");
                    retBuffer.append("<configuration>");
                    retBuffer.append("<title>" + layer.getName() + ": " + displayField + "</title>");
                    retBuffer.append("<description>");
                    if (featureLayer.getHotlinkType() == esriHyperlinkType.esriHyperlinkTypeURL) {
                        String hotLinkField = featureLayer.getHotlinkField();
                        retBuffer.append("<a href=\"{" + hotLinkField + "}\">HotLink</a>");
                    }
                    retBuffer.append("</description>");
                    IFieldInfoSet fieldInfoSet = featureLayer.getFieldInfos();
                    IFields fields = featureLayer.getFields();
                    retBuffer.append("<fields>");
                    HashMap<String, String> mediaFieldNames = new HashMap<String, String>();
                    for (int i = 0; i < fields.getFieldCount(); i++) {
                        IField field = fields.getField(i);
                        String name = field.getName();
                        logger.debug(TAG, "calculatePopup(" + layerId + ").field[" + i + "].name = " + name);
                        IFieldInfo fieldInfo = fieldInfoSet.find(name);
                        String alias = fieldInfo.getAlias();
                        logger.debug(TAG, "calculatePopup(" + layerId + ").field[" + i + "].alias = " + alias);
                        if (alias.startsWith("_media_")) {
                            mediaFieldNames.put(alias, name);
                        } else {
                            if (fieldInfo.isVisible() || name.equals(displayField) || alias.equals(displayField)) {
                                //String visible = (name.equals(displayField) || alias.equals(displayField) ? "visible=\"false\" " : "visible=\"" + fieldInfo.isVisible() + "" + "\" ");
                                int fieldType = field.getType();
                                retBuffer.append(generateFieldElement(name, alias, fieldInfo, fieldType, field, layer));
                            }
                        }
                    }
                    retBuffer.append("</fields>");

                    if (mediaFieldNames.size() > 0) {
                        Set<String> mediaFieldAliases = mediaFieldNames.keySet();
                        retBuffer.append("<medias>");
                        for (String mediaFieldAlias : mediaFieldAliases) {
                            if (mediaFieldAlias.startsWith("_media_image_") && mediaFieldAlias.endsWith("_source")) {
                                String fieldSourceName = mediaFieldNames.get(mediaFieldAlias);
                                String imageSource = "{" + fieldSourceName + "}";
                                String imageLink = imageSource;
                                String mediaLinkFieldAlias = mediaFieldAlias.substring(0, mediaFieldAlias.length() - 7) + "_link";
                                if (mediaFieldNames.containsKey(mediaLinkFieldAlias)) {
                                    String fieldLinkName = mediaFieldNames.get(mediaLinkFieldAlias);
                                    imageLink = "{" + fieldLinkName + "}";
                                }
                                retBuffer.append("<media type=\"image\" imagesource=\"" + imageSource + "\" imagelink=\"" + imageLink + "\" /> ");
                            } else if (mediaFieldAlias.startsWith("_media_image_") && mediaFieldAlias.endsWith("_link")) {
                                //ignore, is processed in _media_image_...._source
                            } else if (mediaFieldAlias.startsWith("_media_") && mediaFieldAlias.endsWith("chart_1_fld")) {
                                String chartType = mediaFieldAlias.substring(7, mediaFieldAlias.length() - 6);
                                String chartFields = mediaFieldNames.get(mediaFieldAlias) + ",";
                                String nextFieldName = null;
                                int fldCounter = 2;
                                while ((nextFieldName = mediaFieldNames.get("_media_" + chartType + "_" + fldCounter + "_fld")) != null) {
                                    chartFields += nextFieldName + ",";
                                    fldCounter++;
                                }
                                if (chartFields.endsWith(","))
                                    chartFields = chartFields.substring(0, chartFields.length() - 1);
                                retBuffer.append("<media type=\"" + chartType + "\" chartFields=\"" + chartFields + "\"/> ");
                            } else if (mediaFieldAlias.startsWith("_media_") && mediaFieldAlias.contains("chart_") && mediaFieldAlias.endsWith("_fld")) {
                                //ignore, is processed in _media_xxxxxchart_xxx_fld
                            } else {
                                logger.error(TAG, "unknown mediaFieldAlias TYPE: " + mediaFieldAlias);
                            }

                        }
                        retBuffer.append("</medias>");
                    }
                    ITableAttachments tableAttachments = new ITableAttachmentsProxy(featureLayer.getFeatureClass());
                    IFeatureClass tmpFeatureClass = featureLayer.getFeatureClass();
                    boolean hasAttachments = false;
                    IEnumRelationshipClass relationshipClasses = tmpFeatureClass.getRelationshipClasses(esriRelRole.esriRelRoleAny);
                    relationshipClasses.reset();
                    IRelationshipClass relationshipClass;
                    while ((relationshipClass = relationshipClasses.next()) != null) {
                        String tmp = relationshipClass.getForwardPathLabel();
                        if (tmp.equals("attachment")) {
                            hasAttachments = true;
                            break;
                        }
                        System.out.println("tmp = " + tmp);
                    }
                    retBuffer.append(hasAttachments ? "<showattachments>true</showattachments>" : "<showattachments>false</showattachments>");
                    retBuffer.append("</configuration>");
                } else {
                    retBuffer.append("unknown layerClass: " + layer);
                    logger.warning(TAG, "calculatePopup: unknown layerClass: " + layer);
                }
            } else {
                logger.warning(TAG, "calculatePopup: No layers found, nothing to do.");
            }
            mapReader.close();
            logger.debug(TAG, "calculatePopup.retBuffer", retBuffer.toString());
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retBuffer.toString();
    }

    private String generateFieldElement(String name, String alias, IFieldInfo fieldInfo, int fieldType, IField field, ILayer layer) throws IOException {
        String visible = "visible=\"" + fieldInfo.isVisible() + "" + "\"";
        StringBuilder retBuffer = new StringBuilder();
        if (alias == null || alias.equals(name)) {
            alias = "";
        } else alias = "alias=\"" + alias + "\" ";

        switch (fieldType) {
            case esriFieldType.esriFieldTypeSmallInteger:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + ">");
                retBuffer.append("<format precision=\"-1\" usethousandsseparator=\"false\"/>");
                retBuffer.append("</field>");
                break;
            case esriFieldType.esriFieldTypeInteger:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + ">");
                retBuffer.append("<format precision=\"-1\" usethousandsseparator=\"false\"/>");
                retBuffer.append("</field>");
                break;
            case esriFieldType.esriFieldTypeSingle:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + ">");
                retBuffer.append("<format precision=\"-1\" usethousandsseparator=\"false\"/>");
                retBuffer.append("</field>");
                break;
            case esriFieldType.esriFieldTypeDouble:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + ">");
                int precision = 3;
                boolean thousandsSeparator = false;
                if (fieldInfo.getNumberFormat() instanceof NumericFormat) {
                    NumericFormat numericFormat = (NumericFormat) fieldInfo.getNumberFormat();
                    precision = numericFormat.getRoundingValue();
                    thousandsSeparator = numericFormat.isUseSeparator();
                }
                retBuffer.append("<format precision=\"" + precision + "\" usethousandsseparator=\"" + thousandsSeparator + "\"/>");
                retBuffer.append("</field>");
                break;
            case esriFieldType.esriFieldTypeString:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + "/>");
                break;
            case esriFieldType.esriFieldTypeDate:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + ">");
                retBuffer.append("<format dateformat=\"shortDateShortTime\"/>");
                retBuffer.append("</field>");
                break;
            case esriFieldType.esriFieldTypeOID:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + ">");
                retBuffer.append("<format precision=\"-1\" usethousandsseparator=\"false\"/>");
                retBuffer.append("</field>");
                break;
            case esriFieldType.esriFieldTypeGeometry:
                break;
            case esriFieldType.esriFieldTypeBlob:
                break;
            case esriFieldType.esriFieldTypeRaster:
                break;
            case esriFieldType.esriFieldTypeGUID:
                break;
            case esriFieldType.esriFieldTypeGlobalID:
                break;
            case esriFieldType.esriFieldTypeXML:
                break;
            default:
                retBuffer.append("<field name=\"" + name + "\" " + alias + visible + "/>");
                break;
        }
        return retBuffer.toString();
    }
}
