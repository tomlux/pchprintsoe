developed by tom@schuller.lu

PCHPrintSOE (http://bitbucket.schuller.lu/pchprintsoe):
  check also the FLexViewer widgets
     http://bitbucket.schuller.lu/pchprintwidget   (FlexViewer2.5)
     http://bitbucket.schuller.lu/pchprintwidget30 (FlexViewer3.0)

Features:
 - full print over the rest api
 - make print on "template" mapservice by keeping the layout
 - multi-mapservices support
 - add graphics on the map view positioned in map units
 - add elements on the layout view positioned in paper units
 - on each mapservice restart, the generated files will be deleted


Quickstart:
 - enable the soe for your mapservice
    - on each mapservice restart, for each layer an .lyr file will be created in the AGS_output directory
    - on a print with the PChPrintSOE, the .lyr file be used, so the NATIVE datasource

 - to enable a mapservice to execute a "print"-rest-function on it, enable the "printMap" operation on the SOE
 
Rest-Doku:
 - see the META-INF/docu.html

Build using Maven:

You can build the project using maven (maven3 has been tested, but maven2 should work just fine).
However you need to have arcobjects in your repository, which is in no public repository i know of.
So you have to install arcobjects into your local repository yourself. Get your arcobjects.jar
and 
mvn install:install-file -Dfile=arcobjects.jar -DgroupId=com.esri.arcgis -DartifactId=arcobjects -Dversion=10.0.2 -Dpackaging=jar